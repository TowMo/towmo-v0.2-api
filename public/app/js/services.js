angular.module('services', ['ngResource', 'ngSanitize', 'lbServices'])
	.factory('DateTimeService', function() {

		return {
			now: function(d) {
				return moment().format('YYYY-MM-DD h:mm:ss');
			}
		};
	})
	.factory('SessionService', function() {
		var sessionObject = {
			get: function(key) {
				var value = sessionStorage.getItem(key);
				return value;
			},
			set: function(key, value) {
				sessionStorage.setItem(key, value);
			},
			unset: function(key) {
				sessionStorage.removeItem(key);
			}
		};

		return sessionObject;
	})
	.factory('AuthenService', function($rootScope, $location, $sanitize, User, SessionService, FlashService) {
		//private functions
		var getSession = function() {
			return SessionService.get("Authenticated");
		}
		var setSession = function() {
			SessionService.set("Authenticated", true);
		};

		var removeSession = function() {
			SessionService.unset("Authenticated");	
		};

		var sanitizeCredentials = function(credentials) {
			return {
				username: $sanitize(credentials.username),
				password: $sanitize(credentials.password)
			}
		};

		//public functions
		return {
			login: function(credentials) {
				var sanitizedCredentials = sanitizeCredentials(credentials);
				var results = User.login({include: 'user', remember: true}, sanitizedCredentials,
						function() {
							setSession();
						}, function(err) {
							loginError(FlashService.show(err.data.error.message));
					});

				return results;
			},

			logout: function() {
				
				removeSession();

				User.logout();
				FlashService.show("See ya soon!");
				$location.path('/login');
			},

			register: function(userInfo) {
				//do something
			},

			isLoggedIn: function() {
				return getSession();
			},

			setCurrentUser: function(user) {
				$rootScope.user = user;
			}
		};
	})
	.factory('FlashService', function($rootScope) {
		return {
			show: function(message) {
				$rootScope.flash = message;
			},

			clear: function() {
				$rootScope.flash = "";
			}
		};
	})
	.factory('ReqSitesDataService', function($q, ReqEmpDuty, $http) {
		var reqSites = [];

		return {
			fetchReqSites: function(userId) {
				var deferred = $q.defer();
				
				$http.get("/api/reqEmpDuties?filter[where][reqEmpId]="
							+userId
							+"&filter[where][isActive]=1"
							+"&filter[where][reqDutyId]=1"
							+"&filter[include]=reqSite")
					.success(function(successResults) {
						deferred.resolve(reqSites = successResults);
					})
					.error(function(error) {
						deferred.resolve(error);
					})

				return deferred.promise;
			}
		}
	})
	.factory('APITestService', function($http) {
		var tables = ['states', 'towSites', 'towSiteGeos', 'towVehicles', 'towEmps',
					'towDuties', 'towEmpDuties', 'vehicleTypes', 'vehicles', 'reqSites',
					'parkingSigns', 'reqSiteGeos', 'towReqs', 'reqEmps', 'reqEmpDuties',
					'statuses', 'statusEntries', 'reqPictures', 'pictureTypes'];
		return {
			getTowOrgs: function() {
				_.each(tables, function(table) {
					console.log($http.get('/api/' +table));
				})
				// return //return a promise
			}
		}
	})
	.factory('TowEmpDutiesDataService', function($http, $q) {
		var towEmpDuties = [];

		return {
			fetchTowEmpDuties: function(currentUserId) {
				var deferred = $q.defer();

				$http.get("/api/towEmpDuties?filter[where][towEmpId]=" +currentUserId)
					.success(function(results) {
						towEmpDuties = results;
						deferred.resolve(results)
					})
					.error(function(errResults) {
						deferred.reject(errResults.data);
					})

				// return  deferred.promise;
			},

			getTowEmpDuty: function(towDutyId, towSiteId) {
				console.log("TowEmpDuties: ", towEmpDuties);
				console.log("TowSiteId: ", towSiteId);
				return _.where(towEmpDuties, {"towDutyId": towDutyId, "towSiteId": towSideId});
			}
		}
		
	})
	.factory('TowSitesDataService', function($http, $q) {
		var towSites = []
		var extractTowSiteIds = function(towSitesArray) {
			return _.map(towSitesArray, function(towSite) {
							
							towSites.push(towSite.towSite);
							return towSite.towSite.id
						})
		};
		
		return {
			fetchTowSiteIds: function(towEmpId) {
				var deferred = $q.defer();
			
				$http.get("/api/towEmpDuties?filter[where][towEmpId]=" +towEmpId +"&filter[include][towSite]=towSiteGeos")
					.success(function(successResults) {
						deferred.resolve( extractTowSiteIds(successResults) );
					})
					.error(function(errResults, status) {
						deferred.reject(errResults.data);
					})

				return deferred.promise;
			},
			getTowSites: function() {
				return towSites;
			}
		}
	})
	.factory('TowReqsDataService', function($http, $q, PictureType, TowReq) {
		var towReqs = [];
		var pictureTypes = [];

		var extractTowReqIds = function(towReqs) {
			var towReqIds = [];
					
			_.each(towReqs, function(record) {
				_.each(record.towReqs, function(towReq) {
					towReqIds.push(towReq.id);
				})
			});

			return towReqIds;
		};

		fetchDetailedTowReqs = function(towReqIds) {
			var deferred = $q.defer();

			$http.get("/api/towReqs?filter[include][reqSite][parkingSigns]"
						+"&filter[where][id][inq]=" +towReqIds
						+"&filter[include][statusEntries][towEmpDuty]=towEmp"
						+"&filter[include][reqPictures]=pictureType"
						+"&filter[include][vehicle]")
				.success(function(results) {
					deferred.resolve(results);
				})
				.error(function(errResults, status) {
					deferred.reject(errResults);
				})

			return deferred.promise
		};

		return {
			fetchTowReqs: function(towSiteIds) {
				var towReqIds = [];
				var deferred = $q.defer();

				// GET TOWREQS
				$http.get("/api/reqSites?filter[where][towSiteId][inq]=" +towSiteIds +"&filter[include]=towReqs")
					.success(function(successResults) {
						towReqIds = extractTowReqIds(successResults);

						if (towReqIds.length > 0) {
							fetchDetailedTowReqs(towReqIds)
								.then(function(results) {
									deferred.resolve(towReqs = results);
								}, function(errResults) {
									console.log("Couldn't get TowReqs: ", errResults);
								})	
						}
					})
					.error(function(errResults, status) {
						deferred.reject(errResults.data);
					});

				return deferred.promise;
			},

			getTowReqs: function() {
				return towReqs;
			},

			addReqPicture: function(reqPicture) {
				var deferred = $q.defer();

				$http.post("/api/reqPictures", reqPicture)
					.success(function(successResults) {
						deferred.resolve(successResults)
					})
					.error(function(errResults) {
						deferred.reject(errResults);
					});

				return deferred.promise;
			},

			fetchPictureTypes: function() {
				var deferred = $q.defer();
				
				$http.get("/api/pictureTypes")
					.success(function(results) {
						deferred.resolve(pictureTypes = results);
					}).error(function(error) {
						deferred.reject(error);
					});

				return deferred.promise;
			},

			createTowReq: function(towReqInfo) {
				var deferred = $q.defer();

				TowReq.create(towReqInfo, function(result) {
					deferred.resolve(result);
				}, function(error) {
					deferred.reject(error);
				})

				return deferred.promise;
			},

			enrichReqPictures: function(reqPicturesArray) {
				reqPicturesArray = _.map(reqPicturesArray, function(reqPicture) {
									if ( (reqPicture != undefined) || (reqPicture != null) ) {
										reqPicture.towReqId = reqPicturesArray.towReqId
										reqPicture.filename = reqPicturesArray.towReqId +"-" +reqPicture.pictureTypeId +".jpg";
										reqPicture.img = null;
									}

									return reqPicture;
								})
				return reqPicturesArray;
			}
		}
	})
	.factory('StatusesDataService', function($http, $q, StatusEntry) {
		var statusEntries = [];
		var statuses = [];
		
		var extractStatusEntries = function(towReqs) {
			return _.map(towReqs, function(towReq) {
								return towReq.statusEntries[0];
							});
		};

		return {
			fetchStatuses: function() {
				var deferred = $q.defer();

				$http.get("/api/statuses")
					.success(function(results) {
						deferred.resolve(statuses = results);
					})
					.error(function(errResults) {
						deferred.reject(errResults.data);
					});

				return deferred.promise;
			},

			setStatuses: function(statusesArray) {
				statuses = statusesArray;
			},

			setStatusEntries: function(towReqs) {
				statusEntries = extractStatusEntries(towReqs);
			},

			getStatusEntries: function() {
				return statusEntries;
			},

			getStatuses: function() {
				return statuses;
			},

			getStatus: function(statusId) {
				return _.findWhere(statuses, {id: statusId});
			},

			createStatusEntry: function(statusEntryInfo) {
				var deferred = $q.defer();

				StatusEntry.create(statusEntryInfo, function(statusEntryResult) {
					deferred.resolve(true);	
				}, function(statusEntryErr) {
					deferred.reject(statusEntryErr);
				});

				return deferred.promise;
			}
		}
	})
	.factory('VehiclesDataService', function($http, $q, Vehicle) {
		var vehicleTypes = [];

		return {
			getIntVehicleData: function (vin) {
				var deferred = $q.defer();

console.log("checking internal DB...");
				Vehicle.find({	filter: {	where: {vin: vin}}},
					function(vinResult) {
						deferred.resolve(vinResult);
					}, function(vinResultErr) {
						deferred.reject(vinResultErr.data);
					});

				return deferred.promise;
			},

			getExtVehicleData: function (vin) {
				var deferred = $q.defer();
				
				// returns an object (instead of a $promise)
				$http.get("https://api.edmunds.com/api/vehicle/v2/vins/"
					+vin
					+"?fmt=json&api_key=dq3n4f4xrm6yky44yu23vn6p")
					.success(function(results) {
						var extVehicleInfo;

console.log("checking Edmunds...");
						if (results.hasOwnProperty('warning')) {
							extVehicleInfo = {vin: vin};
						} else {
							extVehicleInfo = {
								id: null,
								make: results.make.name,
								model: results.model.name,
								color: null,
								year: results.years[0].year,
								transmission: results.transmission.transmissionType,
								vehicleTypeId: results.categories.primaryBodyType,
								vin: results.vin,
								licenseNum: null
							};
							
							extVehicleInfo.vehicleTypeId = _.findWhere(vehicleTypes, {name: extVehicleInfo.vehicleTypeId}).id;
						}

						deferred.resolve(extVehicleInfo);
					})
					.error(function(errResults) {
						deferred.reject(errResults);
					});

				return deferred.promise;
			},

			fetchVehicleTypes: function() {
				var deferred = $q.defer();

				$http.get("/api/vehicleTypes")
					.success(function(results) {
						deferred.resolve(vehicleTypes = results)
					})
					.error(function(errResults) {
						deferred.reject(errResults.data);
					})

				return deferred.promise;
			},

			getVehicleTypes: function() {
				return vehicleTypes;
			},

			getVehicleType: function(vehicleTypeId) {
				return _.findWhere(vehicleTypes, {id: vehicleTypeId});	
			},

			setVehicleTypes: function(vehicleTypesArray) {
				vehicleTypes = vehicleTypesArray;
			},

			createVehicle: function(vehicleInfo) {
				var deferred = $q.defer();

				Vehicle.create(vehicleInfo, function(results) {
					deferred.resolve(results.$promise);
				}, function(errStatus) {
					deferred.reject(errStatus.data);
				});

				return deferred.promise;
			}
		}
	})
	.factory('LocationsDataService', function($q, $http) {
		var mapMarkers = [];

		var buildLocationPoints = function(nodes) {
			return _.map(nodes, function(node) {
				return new google.maps.LatLng(node.lat, node.long);
			});
		};

		return {
			getCurrentLocation: function() {
				var deferred = $q.defer();

				navigator.geolocation.getCurrentPosition(function(geoInfo) {
					deferred.resolve(geoInfo);
				}, function(err) {
					deferred.reject(err);
				});
				
				return deferred.promise;
			},

			checkDistances: function(targetGeos) {
				var deferred = $q.defer();

				this.getCurrentLocation()
					.then(function(results) {
						// GET CURRENT LOCATION
						return [{lat: results.coords.latitude, long: results.coords.longitude}];
					}, function(errResults) {
						deferred.reject( errResults);
					})
					.then(function(originGeo) {
						// BUILD DSITANCE MATRIX PARAMETERS
						return {
							origins: buildLocationPoints(originGeo),
							destinations: buildLocationPoints(targetGeos),
							travelMode: google.maps.TravelMode.DRIVING,
							unitSystem: google.maps.UnitSystem.METRIC,
							avoidHighways: false,
							avoidTolls: false
						}
					})
					.then(function(dmParams) {
						// GET CALCULATED DISTANCES
						var distanceService = new google.maps.DistanceMatrixService();
						
						distanceService.getDistanceMatrix(dmParams, function(response, status) {
							if (status != google.maps.DistanceMatrixStatus.OK) {
								deferred.reject(status);
							} else {
								deferred.resolve(_.pluck(response.rows[0].elements, "distance"));
							}
						})
					}, function(errResults) {
						deferred.reject(errResults);
					});

				return deferred.promise;
			}
		}
	})
	.factory('$fileUploader', [ '$compile', '$rootScope', '$http', '$window', function ($compile, $rootScope, $http, $window) {
	    'use strict';

	    /**
	     * Creates a uploader
	     * @param {Object} params
	     * @constructor
	     */
	    function Uploader(params) {
	        angular.extend(this, {
	            scope: $rootScope,
	            url: '/',
	            alias: 'file',
	            queue: [],
	            headers: {},
	            progress: null,
	            autoUpload: false,
	            removeAfterUpload: false,
	            method: 'POST',
	            filters: [],
	            formData: [],
	            isUploading: false,
	            _nextIndex: 0,
	            _timestamp: Date.now()
	        }, params);

	        // add the base filter
	        this.filters.unshift(this._filter);

	        this.scope.$on('file:add', function (event, items, options) {
	            event.stopPropagation();
	            this.addToQueue(items, options);
	        }.bind(this));

	        this.bind('beforeupload', Item.prototype._beforeupload);
	        this.bind('in:progress', Item.prototype._progress);
	        this.bind('in:success', Item.prototype._success);
	        this.bind('in:cancel', Item.prototype._cancel);
	        this.bind('in:error', Item.prototype._error);
	        this.bind('in:complete', Item.prototype._complete);
	        this.bind('in:progress', this._progress);
	        this.bind('in:complete', this._complete);
	    }

	    Uploader.prototype = {
	        /**
	         * Link to the constructor
	         */
	        constructor: Uploader,

	        /**
	         * The base filter. If returns "true" an item will be added to the queue
	         * @param {File|Input} item
	         * @returns {boolean}
	         * @private
	         */
	        _filter: function (item) {
	            return angular.isElement(item) ? true : !!item.size;
	        },

	        /**
	         * Registers a event handler
	         * @param {String} event
	         * @param {Function} handler
	         * @return {Function} unsubscribe function
	         */
	        bind: function (event, handler) {
	            return this.scope.$on(this._timestamp + ':' + event, handler.bind(this));
	        },

	        /**
	         * Triggers events
	         * @param {String} event
	         * @param {...*} [some]
	         */
	        trigger: function (event, some) {
	            arguments[ 0 ] = this._timestamp + ':' + event;
	            this.scope.$broadcast.apply(this.scope, arguments);
	        },

	        /**
	         * Checks a support the html5 uploader
	         * @returns {Boolean}
	         * @readonly
	         */
	        isHTML5: !!($window.File && $window.FormData),

	        /**
	         * Adds items to the queue
	         * @param {FileList|File|HTMLInputElement} items
	         * @param {Object} [options]
	         */
	        addToQueue: function (items, options) {
	            var length = this.queue.length;
	            var list = 'length' in items ? items : [items];

	            angular.forEach(list, function (file) {
	                // check a [File|HTMLInputElement]
	                var isValid = !this.filters.length ? true : this.filters.every(function (filter) {
	                    return filter.call(this, file);
	                }, this);

	                // create new item
	                var item = new Item(angular.extend({
	                    url: this.url,
	                    alias: this.alias,
	                    headers: angular.copy(this.headers),
	                    formData: angular.copy(this.formData),
	                    removeAfterUpload: this.removeAfterUpload,
	                    method: this.method,
	                    uploader: this,
	                    file: file
	                }, options));

	                if (isValid) {
	                    this.queue.push(item);
	                    this.trigger('afteraddingfile', item);
	                } else {
	                    this.trigger('whenaddingfilefailed', item);
	                }
	            }, this);

	            if (this.queue.length !== length) {
	                this.trigger('afteraddingall', this.queue);
	                this.progress = this._getTotalProgress();
	            }

	            this._render();
	            this.autoUpload && this.uploadAll();
	        },

	        /**
	         * Remove items from the queue. Remove last: index = -1
	         * @param {Item|Number} value
	         */
	        removeFromQueue: function (value) {
	            var index = this.getIndexOfItem(value);
	            var item = this.queue[ index ];
	            item.isUploading && item.cancel();
	            this.queue.splice(index, 1);
	            item._destroy();
	            this.progress = this._getTotalProgress();
	        },

	        /**
	         * Clears the queue
	         */
	        clearQueue: function () {
	            this.queue.forEach(function (item) {
	                item.isUploading && item.cancel();
	                item._destroy();
	            }, this);
	            this.queue.length = 0;
	            this.progress = 0;
	        },

	        /**
	         * Returns a index of item from the queue
	         * @param {Item|Number} value
	         * @returns {Number}
	         */
	        getIndexOfItem: function (value) {
	            return angular.isObject(value) ? this.queue.indexOf(value) : value;
	        },

	        /**
	         * Returns not uploaded items
	         * @returns {Array}
	         */
	        getNotUploadedItems: function () {
	            return this.queue.filter(function (item) {
	                return !item.isUploaded;
	            });
	        },

	        /**
	         * Returns items ready for upload
	         * @returns {Array}
	         */
	        getReadyItems: function() {
	            return this.queue
	                .filter(function(item) {
	                    return item.isReady && !item.isUploading;
	                })
	                .sort(function(item1, item2) {
	                    return item1.index - item2.index;
	                });
	        },

	        /**
	         * Uploads a item from the queue
	         * @param {Item|Number} value
	         */
	        uploadItem: function (value) {
	            var index = this.getIndexOfItem(value);
	            var item = this.queue[ index ];
	            var transport = this.isHTML5 ? '_xhrTransport' : '_iframeTransport';

	            item.index = item.index || this._nextIndex++;
	            item.isReady = true;

	            if (this.isUploading) {
	                return;
	            }

	            this.isUploading = true;
	            this[ transport ](item);
	        },

	        /**
	         * Cancels uploading of item from the queue
	         * @param {Item|Number} value
	         */
	        cancelItem: function(value) {
	            var index = this.getIndexOfItem(value);
	            var item = this.queue[ index ];
	            var prop = this.isHTML5 ? '_xhr' : '_form';
	            item[prop] && item[prop].abort();
	        },

	        /**
	         * Uploads all not uploaded items of queue
	         */
	        uploadAll: function () {
	            var items = this.getNotUploadedItems().filter(function(item) {
	                return !item.isUploading;
	            });
	            items.forEach(function(item) {
	                item.index = item.index || this._nextIndex++;
	                item.isReady = true;
	            }, this);
	            items.length && this.uploadItem(items[ 0 ]);
	        },

	        /**
	         * Cancels all uploads
	         */
	        cancelAll: function() {
	            this.getNotUploadedItems().forEach(function(item) {
	                this.cancelItem(item);
	            }, this);
	        },

	        /**
	         * Updates angular scope
	         * @private
	         */
	        _render: function() {
	            this.scope.$$phase || this.scope.$digest();
	        },

	        /**
	         * Returns the total progress
	         * @param {Number} [value]
	         * @returns {Number}
	         * @private
	         */
	        _getTotalProgress: function (value) {
	            if (this.removeAfterUpload) {
	                return value || 0;
	            }

	            var notUploaded = this.getNotUploadedItems().length;
	            var uploaded = notUploaded ? this.queue.length - notUploaded : this.queue.length;
	            var ratio = 100 / this.queue.length;
	            var current = (value || 0) * ratio / 100;

	            return Math.round(uploaded * ratio + current);
	        },

	        /**
	         * The 'in:progress' handler
	         * @private
	         */
	        _progress: function (event, item, progress) {
	            var result = this._getTotalProgress(progress);
	            this.trigger('progressall', result);
	            this.progress = result;
	            this._render();
	        },

	        /**
	         * The 'in:complete' handler
	         * @private
	         */
	        _complete: function () {
	            console.log("inside complete function")
	            var item = this.getReadyItems()[ 0 ];
	            this.isUploading = false;

	            if (angular.isDefined(item)) {
	                this.uploadItem(item);
	                return;
	            }

	            this.trigger('completeall', this.queue);
	            this.progress = this._getTotalProgress();
	            this._render();
	        },

	        /**
	         * The XMLHttpRequest transport
	         * @private
	         */
	        _xhrTransport: function (item) {
	            var xhr = item._xhr = new XMLHttpRequest();
	            var form = new FormData();
	            var that = this;

	            this.trigger('beforeupload', item);

	            item.formData.forEach(function(obj) {
	                angular.forEach(obj, function(value, key) {
	                    form.append(key, value);
	                });
	            });

	            form.append(item.file.name, item.file);
	            // form.append(item.alias, item.file);
	           	console.log("FORM: ", form);

	            xhr.upload.onprogress = function (event) {
	                var progress = event.lengthComputable ? event.loaded * 100 / event.total : 0;
	                that.trigger('in:progress', item, Math.round(progress));
	            };

	            xhr.onload = function () {
	                var response = that._transformResponse(xhr.response);
	                var event = that._isSuccessCode(xhr.status) ? 'success' : 'error';
	                that.trigger('in:' + event, xhr, item, response);
	                that.trigger('in:complete', xhr, item, response);
	            };

	            xhr.onerror = function () {
	                that.trigger('in:error', xhr, item);
	                that.trigger('in:complete', xhr, item);
	            };

	            xhr.onabort = function () {
	                that.trigger('in:cancel', xhr, item);
	                that.trigger('in:complete', xhr, item);
	            };

	            xhr.open(item.method, item.url, true);

	            angular.forEach(item.headers, function (value, name) {
	                xhr.setRequestHeader(name, value);
	            });

console.log("FORM*****: ", form);

	            xhr.send(form);
	        },

	        /**
	         * The IFrame transport
	         * @private
	         */
	        _iframeTransport: function (item) {
	            var form = angular.element('<form style="display: none;" />');
	            var iframe = angular.element('<iframe name="iframeTransport' + Date.now() + '">');
	            var input = item._input;
	            var that = this;

	            item._form && item._form.replaceWith(input); // remove old form
	            item._form = form; // save link to new form

	            this.trigger('beforeupload', item);

	            input.prop('name', item.alias);

	            item.formData.forEach(function(obj) {
	                angular.forEach(obj, function(value, key) {
	                    form.append(angular.element('<input type="hidden" name="' + key + '" value="' + value + '" />'));
	                });
	            });

	            form.prop({
	                action: item.url,
	                method: item.method,
	                target: iframe.prop('name'),
	                enctype: 'multipart/form-data',
	                encoding: 'multipart/form-data' // old IE
	            });

	            iframe.bind('load', function () {
	                // fixed angular.contents() for iframes
	                var html = iframe[0].contentDocument.body.innerHTML;
	                var xhr = { response: html, status: 200, dummy: true };
	                var response = that._transformResponse(xhr.response);
	                that.trigger('in:success', xhr, item, response);
	                that.trigger('in:complete', xhr, item, response);
	            });

	            form.abort = function() {
	                var xhr = { status: 0, dummy: true };
	                iframe.unbind('load').prop('src', 'javascript:false;');
	                form.replaceWith(input);
	                that.trigger('in:cancel', xhr, item);
	                that.trigger('in:complete', xhr, item);
	            };

	            input.after(form);
	            form.append(input).append(iframe);

	            form[ 0 ].submit();
	        },

	        /**
	         * Checks whether upload successful
	         * @param {Number} status
	         * @returns {Boolean}
	         * @private
	         */
	        _isSuccessCode: function(status) {
	            return (status >= 200 && status < 300) || status === 304;
	        },

	        /**
	         * Transforms the server response
	         * @param {*} response
	         * @returns {*}
	         * @private
	         */
	        _transformResponse: function (response) {
	            $http.defaults.transformResponse.forEach(function (transformFn) {
	                response = transformFn(response);
	            });
	            return response;
	        }
	    };


	    /**
	     * Create a item
	     * @param {Object} params
	     * @constructor
	     */
	    function Item(params) {
	        // fix for old browsers
	        if (!Uploader.prototype.isHTML5) {
	            var input = angular.element(params.file);
	            var clone = $compile(input.clone())(params.uploader.scope);
	            var value = input.val();

	            params.file = {
	                lastModifiedDate: null,
	                size: null,
	                type: 'like/' + value.slice(value.lastIndexOf('.') + 1).toLowerCase(),
	                name: value.slice(value.lastIndexOf('/') + value.lastIndexOf('\\') + 2)
	            };

	            params._input = input;
	            clone.prop('value', null); // FF fix
	            input.css('display', 'none').after(clone); // remove jquery dependency
	        }

	        angular.extend(this, {
	            isReady: false,
	            isUploading: false,
	            isUploaded: false,
	            isSuccess: false,
	            isCancel: false,
	            isError: false,
	            progress: null,
	            index: null
	        }, params);
	    }


	    Item.prototype = {
	        /**
	         * Link to the constructor
	         */
	        constructor: Item,
	        /**
	         * Removes a item
	         */
	        remove: function () {
	            this.uploader.removeFromQueue(this);
	        },
	        /**
	         * Uploads a item
	         */
	        upload: function () {
	            this.uploader.uploadItem(this);
	        },
	        /**
	         * Cancels uploading
	         */
	        cancel: function() {
	            this.uploader.cancelItem(this);
	        },
	        /**
	         * Destroys form and input
	         * @private
	         */
	        _destroy: function() {
	            this._form && this._form.remove();
	            this._input && this._input.remove();
	            delete this._form;
	            delete this._input;
	        },
	        /**
	         * The 'beforeupload' handler
	         * @param {Object} event
	         * @param {Item} item
	         * @private
	         */
	        _beforeupload: function (event, item) {
	            item.isReady = true;
	            item.isUploading = true;
	            item.isUploaded = false;
	            item.isSuccess = false;
	            item.isCancel = false;
	            item.isError = false;
	            item.progress = 0;
	        },
	        /**
	         * The 'in:progress' handler
	         * @param {Object} event
	         * @param {Item} item
	         * @param {Number} progress
	         * @private
	         */
	        _progress: function (event, item, progress) {
	            item.progress = progress;
	            item.uploader.trigger('progress', item, progress);
	        },
	        /**
	         * The 'in:success' handler
	         * @param {Object} event
	         * @param {XMLHttpRequest} xhr
	         * @param {Item} item
	         * @param {*} response
	         * @private
	         */
	        _success: function (event, xhr, item, response) {
	            item.isReady = false;
	            item.isUploading = false;
	            item.isUploaded = true;
	            item.isSuccess = true;
	            item.isCancel = false;
	            item.isError = false;
	            item.progress = 100;
	            item.index = null;
	            item.uploader.trigger('success', xhr, item, response);
	        },
	        /**
	         * The 'in:cancel' handler
	         * @param {Object} event
	         * @param {XMLHttpRequest} xhr
	         * @param {Item} item
	         * @private
	         */
	        _cancel: function(event, xhr, item) {
	            item.isReady = false;
	            item.isUploading = false;
	            item.isUploaded = false;
	            item.isSuccess = false;
	            item.isCancel = true;
	            item.isError = false;
	            item.progress = 0;
	            item.index = null;
	            item.uploader.trigger('cancel', xhr, item);
	        },
	        /**
	         * The 'in:error' handler
	         * @param {Object} event
	         * @param {XMLHttpRequest} xhr
	         * @param {Item} item
	         * @param {*} response
	         * @private
	         */
	        _error: function (event, xhr, item, response) {
	            item.isReady = false;
	            item.isUploading = false;
	            item.isUploaded = true;
	            item.isSuccess = false;
	            item.isCancel = false;
	            item.isError = true;
	            item.progress = 100;
	            item.index = null;
	            item.uploader.trigger('error', xhr, item, response);
	        },
	        /**
	         * The 'in:complete' handler
	         * @param {Object} event
	         * @param {XMLHttpRequest} xhr
	         * @param {Item} item
	         * @param {*} response
	         * @private
	         */
	        _complete: function (event, xhr, item, response) {
	            item.uploader.trigger('complete', xhr, item, response);
	            item.removeAfterUpload && item.remove();
	        }
	    };

	    return {
	        create: function (params) {
	            return new Uploader(params);
	        },
	        isHTML5: Uploader.prototype.isHTML5
	    };
	}])