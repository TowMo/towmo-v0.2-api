angular.module('directives', ['controllers', 'services'])
	.directive("towReqSummary", function() {
		return {
			restrict: 'E',
			controller: function($scope, StatusesDataService, LocationsDataService) {
				$scope.addNextStatuses = function (currentStatusId) {
					$scope.nextStatuses = _.where(StatusesDataService.getStatuses(),
												{"prevStatusId": currentStatusId, 
												"userTypeId": 1});

					console.log("Next Statuses: ", $scope.nextStatuses);
					
					// ADD GEO COORDINATES OF TARGET LOCATIONS
					if ($scope.nextStatuses.length > 0) {
						$scope.nextStatuses = _.map($scope.nextStatuses, function(nextStatus) {
											var statusEntry;

											if (nextStatus.proximity) {
												nextStatus.targetGeoCoordinates = {};

												if (nextStatus.id === 8) {
													// GET GEO COORDINATES OF 'TOW SITE' LOCATION
													nextStatus.targetGeoCoordinates = _.findWhere($scope.$parent.towSites, {"id": $scope.towReq.reqSite.towSiteId}).towSiteGeos[0];
												} else if (nextStatus.id === 6) {
													// GET GEO COORDINATES OF 'REQUESTED' STATUS
													statusEntry = _.findWhere($scope.towReq.statusEntries, {"statusId": 2});
													nextStatus.targetGeoCoordinates.lat = statusEntry.lat;
													nextStatus.targetGeoCoordinates.long = statusEntry.long;
												}
											}

											return nextStatus;
										});
						return true;
					} else {
						return false;
					}
				};

				$scope.enrichNextStatuses = function() {
					var targetGeoCoordinatesArray = _.remove(_.pluck($scope.nextStatuses, "targetGeoCoordinates"), function(targetGeoCoordinate) {
														if (targetGeoCoordinate) 
															return targetGeoCoordinate
													});

					LocationsDataService.checkDistances(targetGeoCoordinatesArray)
						.then(function(results) {
							var element = 0;
							$scope.nextStatuses = _.map($scope.nextStatuses, function(nextStatus) {
													if (nextStatus['proximity']) {
														nextStatus.highlight = results[element].value > nextStatus.proximity ? false : true;
														element++;
													} else {
														nextStatus.highlight = true;
													}

													return nextStatus;
												});
						}, function(errResults) {
							console.log("Couldn't find Distances: ", errResults);
						});
				};
			},
			link: function(scope, element, attrs) {
				glowTowReqSummary = false;

				function showTowReqDetails() {
					if(scope.$parent.showTowReqDetails = !scope.$parent.showTowReqDetails) {
						var statusEntries = scope.towReq.statusEntries;
						
						// ADD LIST OF NEXT STATUSES TO CHOSEN TOW REQ
						if ( scope.addNextStatuses(statusEntries[statusEntries.length-1].statusId) ) {
							scope.enrichNextStatuses();
							scope.$parent.nextStatuses = scope.nextStatuses;
						} else {
							scope.$parent.nextStatuses = null;
						}
					}

					scope.$parent.chosenTowReq = scope.towReq;
					scope.$parent.showMapCanvas = !scope.$parent.showMapCanvas;
					scope.$apply();
				};

				function toggleGlowTowReqSummary() {
					glowTowReqSummary = !glowTowReqSummary;					
					element[0].children[0].style.border = glowTowReqSummary ? "1px #FF0000 solid" : ""
				};

				//console.log(element[0].children[0].children[0].children[0].children[0].children[1].childNodes[1].bind('click', showTowReqDetails));
				element.bind('mouseenter', toggleGlowTowReqSummary);
				element.bind('mouseleave', toggleGlowTowReqSummary);
				element.bind('click', showTowReqDetails);
			}
		}
	})
	.directive('mapCanvas', function() {
		return {
			restrict: 'E',
			template: "<div id='map-canvas'><div>",
			scope: {
				statusEntries: "=",
				towSites: "=",
				statuses: "="
			},
			link: function(scope, element, attrs) {
				var currentLoc = {};
				var towReqLocations = [];
				var geoLocations = [];
				var geoMarkers = [];
				var map;

				var createMapMarker = function(position, icon) {
					var marker = new google.maps.Marker({
							position: new google.maps.LatLng(position.latitude, position.longitude),
							icon: icon,
							map: map,
							animation: google.maps.Animation.DROP
						});

					geoMarkers.push(marker);
				}

				var createMapMarkers = function() {
					_.each(geoLocations, function(position) {
						var icon = null;
						createMapMarker(position, icon);
						//google.maps.event.addListener(marker, 'mouseover', toggleBounce);
					});
				}

				var setMap = function(geoObj, callback) {
					//DECLARE MAP BEHAVIOR
					var mapOptions = {
						center: new google.maps.LatLng(geoObj.latitude, geoObj.longitude),
						zoom: 13,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					//INSTATIATE MAP OBJECT
					map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
					var icon = {
						path: google.maps.SymbolPath.CIRCLE,
						scale: 6,
						strokeColor: '#0101DF'
					}

					createMapMarker(geoObj, icon)
					
					//CREATE MAP MARKERS
					callback();
				}

				scope.$watch(function(scopeObjects) {
					if (scopeObjects.statusEntries.length !== 0) {
						geoLocations = [];
						geoLocations = _.map(scopeObjects.statusEntries, function(statusEntry) {
											return {latitude: statusEntry.lat, longitude: statusEntry.long};
										});

						centerPoint = {latitude: scopeObjects.towSites[0].towSiteGeos[0].lat, longitude: scopeObjects.towSites[0].towSiteGeos[0].long};			
						setMap(centerPoint, createMapMarkers);

						// navigator.geolocation.getCurrentPosition(function(position) {
						// 	setMap(position.coords, createMapMarkers);
						// });
					}
				})
			}
		}
	})
	.directive("towReqDetails", function() {
		return {
			restrict: 'E',
			controller: function($scope, StatusesDataService, LocationsDataService, DateTimeService, TowEmpDutiesDataService) {
				$scope.updateStatus = function(nextStatus) {
					console.log($scope);
					LocationsDataService.getCurrentLocation()
						.then(function(results) {
							return results.coords;
						})
						.then(function(geoCoordinates) {
							return {
								statusId: nextStatus.id,
								towEmpDutyId: TowEmpDutiesDataService.getTowEmpDuty(1, $scope.chosenTowReq.reqSite.towSiteId)[0].id,
								towReqId: $scope.chosenTowReq.id,
								timestamp: DateTimeService.now(),
								lat: geoCoordinates.latitude,
								long: geoCoordinates.longitude
							}
						})
						.then(function(statusParams) {
							console.log(statusParams);
							StatusesDataService.createStatusEntry(statusParams);
						});
				}
			},
			link: function(scope, element, attrs) {
				var geoLocations = [];
				var geoMarkers = [];
				var map;

				var createMapMarker = function(position, icon) {
					var marker = new google.maps.Marker({
							position: new google.maps.LatLng(position.latitude, position.longitude),
							icon: icon,
							map: map,
							animation: google.maps.Animation.DROP
						});

					geoMarkers.push(marker);
				}

				var createMapMarkers = function() {
					_.each(geoLocations, function(position) {
						var icon = null;
						createMapMarker(position, icon);
						//google.maps.event.addListener(marker, 'mouseover', toggleBounce);
					});
				}

				var setMap = function(geoObj, callback) {
					//DECLARE MAP BEHAVIOR
					var mapOptions = {
						center: new google.maps.LatLng(geoObj.latitude, geoObj.longitude),
						zoom: 18,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					//INSTATIATE MAP OBJECT
					map = new google.maps.Map(document.getElementById("map-canvas-small"), mapOptions);
					var icon = {
						path: google.maps.SymbolPath.CIRCLE,
						scale: 7,
						strokeColor: '#0101DF'
					}

					createMapMarker(geoObj, icon)
					
					//CREATE MAP MARKERS
					callback();
				}

				// scope.$watch(function(scopeObjects) {
				// 	// console.log("CHOSEN TOW REQ", scopeObjects.chosenTowReq);
				// 	centerPoint = {latitude: scopeObjects.chosenTowReq.statusEntries[0].lat, longitude: scopeObjects.chosenTowReq.statusEntries[0].long};			
				// 	setMap(centerPoint, createMapMarkers);

				// 	geoLocations = [];
				// 	geoLocations = _.map(scopeObjects.chosenTowReq.reqSite.parkingSigns, function(parkingSign) {
				// 						return {latitude: parkingSign.lat, longitude: parkingSign.long};
				// 					});
				// })

				scope.$watch('chosenTowReq', function(chosenTowReq) {
					centerPoint = {latitude: chosenTowReq.statusEntries[0].lat, longitude: chosenTowReq.statusEntries[0].long};			
					setMap(centerPoint, createMapMarkers);

					geoLocations = [];
					geoLocations = _.map(chosenTowReq.reqSite.parkingSigns, function(parkingSign) {
										return {latitude: parkingSign.lat, longitude: parkingSign.long};
									});
				});
			}
		}
	})
	.directive('viewNav', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.on('click', function() {
					element[0].parentNode.html = "<li class='chosen-nav-link'>";				
					scope.viewsToShow = _.map(scope.viewsToShow, function(attr) {
										return attr = false;
									});
					scope.viewsToShow[attrs.value] = true;
					scope.$apply();
				});
			}
		}
	})
	.directive('sitesView', function($templateCache) {
		return {
			restrict: 'E',
			template: $templateCache.get('sites-view.html')[1],
			link: function(scope, element, attrs) {
				
			},
		}
	})
	.directive('empsView', function($templateCache) {
		return {
			restrict: 'E',
			template: $templateCache.get('emps-view.html')[1],
			link: function(scope, element, attrs) {

			},
		}
	})
	.directive('towReqsView', function($route, $templateCache, $fileUploader, VehiclesDataService) {
		return {
			restrict: 'E',
			controller: 'towReqsViewCtrl',
			template: $templateCache.get('tow-reqs-view.html')[1],
			link: function(scope, element, attrs) {
			    var video = document.querySelector('video');

			    var pictureWidth = 395;
			    var pictureHeight = 220;
			    var pictureQuality = 0.9;

			    function checkRequirements() {
			        var deferred = new $.Deferred();

			        //Check if getUserMedia is available
			        if (!Modernizr.getusermedia) {
			            deferred.reject('Your browser doesn\'t support getUserMedia (according to Modernizr).');
			        }

			        //Check if WebGL is available
			        if (Modernizr.webgl) {
			            try {
			                //setup glfx.js
			                fxCanvas = fx.canvas();
			            } catch (e) {
			                deferred.reject('Sorry, glfx.js failed to initialize. WebGL issues?');
			            }
			        } else {
			            deferred.reject('Your browser doesn\'t support WebGL (according to Modernizr).');
			        }

			        deferred.resolve();

			        return deferred.promise();
			    }

			    function searchForRearCamera() {
			        var deferred = new $.Deferred();

			        //MediaStreamTrack.getSources seams to be supported only by Chrome
			        if (MediaStreamTrack && MediaStreamTrack.getSources) {
			            MediaStreamTrack.getSources(function (sources) {
			                var rearCameraIds = sources.filter(function (source) {
			                    return (source.kind === 'video' && source.facing === 'environment');
			                }).map(function (source) {
			                    return source.id;
			                });

			                if (rearCameraIds.length) {
			                    deferred.resolve(rearCameraIds[0]);
			                } else {
			                    deferred.resolve(null);
			                }
			            });
			        } else {
			            deferred.resolve(null);
			        }

			        return deferred.promise();
			    }

			    function setupVideo(rearCameraId) {
			        var deferred = new $.Deferred();
			        var getUserMedia = Modernizr.prefixed('getUserMedia', navigator);
			        var videoSettings = {
			            video: {
			                mandatory: {
			                	maxWidth: pictureWidth,
			                	maxHeight: pictureHeight
			                }
			            }
			        };

			        //if rear camera is available - use it
			        if (rearCameraId) {
			            videoSettings.video.optional.push({
			                sourceId: rearCameraId
			            });
			        }

			        getUserMedia(videoSettings, function (stream) {
			            //Setup the video stream
			            video.src = window.URL.createObjectURL(stream);
			            window.stream = stream;

			            video.addEventListener("loadedmetadata", function (e) {
			                // get video width and height as it might be different than we requested
			                // pictureWidth = this.videoWidth;
			                // pictureHeight = this.videoHeight;

			                if (!pictureWidth && !pictureHeight) {
			                    //firefox fails to deliver info about video size on time (issue #926753), we have to wait
			                    var waitingForSize = setInterval(function () {
			                        if (video.videoWidth && video.videoHeight) {
			                            pictureWidth = video.videoWidth;
			                            pictureHeight = video.videoHeight;

			                            clearInterval(waitingForSize);
			                            deferred.resolve();
			                        }
			                    }, 100);
			                } else {
			                    deferred.resolve();
			                }
			            }, false);
			        }, function (err) {
			        	console.log("Error: ", err);
			            deferred.reject('There is no access to your camera, have you denied it?');
			        });

			        return deferred.promise();
			    }

			    function step1() {
			        checkRequirements()
			            .then(searchForRearCamera)
			            .then(setupVideo)
			            .done(function () {
			                //Enable the 'take picture' button
			                scope.cameraMode = true;
			                $('#takePicture').removeAttr('disabled');
			            })
			            .fail(function (error) {
			                showError(error);
			            });
			    }

			    function step2() {		        
			    	//get very first instances of the selected DOM elements
			        var canvas = document.querySelector('#camera-photos canvas');
			        // var imgTarget = document.querySelector('#camera img');

			        //setup canvas
			        canvas.width = pictureWidth;
			        canvas.height = pictureHeight;

			        var ctx = canvas.getContext('2d');

			        //draw picture from video on canvas
			        ctx.drawImage(video, 0, 0, pictureWidth, pictureHeight);
	
					// Record Picture Information
					scope.takePicture();

					var dataURL = scope.takenPicture.img = canvas.toDataURL('image/jpeg', pictureQuality);
					var blob = dataURItoBlob(dataURL);
					blob.name = "file testing";
					canvas.src = blob;		

					scope.files2 = [];
					scope.files2.push(blob);
					$fileUploader.isHTML5 || element.removeAttr('multiple');

					scope.$emit('file:add', scope.files2, scope.$eval(attrs.ngFileSelect));
					($fileUploader.isHTML5 && element.attr('multiple')) && element.prop('value', null);

					element.prop('value', null); // FF fix
			    }

			    function dataURItoBlob(dataURI) {
			    	'use strict'
			    	var byteString, mimeString;

			    	if(dataURI.split(',')[0].indexOf('base64') !== -1) {
			    		byteString = atob(dataURI.split(',')[1])
			    	} else {
			    		byteString = decodeURI(dataURI.split(',')[1]);
			    	}

			    	mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
			    	var content = _.map(byteString, function(element) {
					    		return element
					    	});

			    	return new Blob([new Uint8Array(content)], {type: mimeString});
			    }

			    /*********************************
			     * UI Stuff
			     *********************************/

			    //start step1 immediately
			    step1();

			    $('#takePicture').on('click', function () {
			        step2();
			    });
			},
		}
	})
	.directive('toggleMediaSection', function() {
		return {
			restrict: 'E',
			template: "<div class='caption'><strong>{{picture.name}}</strong></div><button><img pictureTypeId='{{picture.id}}' src='/app/img/buttons/add.png' /></button>",
			link: function(scope, element, attrs) {
				element.bind('click', function() {
					scope.toggleMode(element[0].children[1]);
					scope.$apply();

				})
			}
		}
	})
	.directive('keepPicture', function() {
		// var timestampEl = angular.element("<div class='caption'>{{geoInfo}}</div>");

		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.bind("click", function(){
					/* set calling Element w/ taken picture */
					scope.callingEl.children[0].src = scope.takenPicture.img;
					
					/* Add || replace currentPicture to reqPictures[] */
					scope.stageReqPicture(scope.takenPicture);
					scope.$apply();
				})
			}
		}
	})
	.directive('enterVin', function() {
		return {
			restrict: 'E',
			template: "VIN: <input type='text' ng-model='vehicle.vin' maxlength='17'/>" +
					"<div class='glyphicon glyphicon-ok' ng-show='input.vin && !input.licNo && !input.vehicle'></div>" +
					"<div class='glyphicon glyphicon-trash' ng-show='input.vin && !input.licNo && !input.vehicle'></div>",
			link: function(scope) {
				scope.$watch('vehicle.vin', function(vin) {
					if (vin.length === 17) {
						scope.getVehicleData(vin, function(result) {
							scope.vehicle = result;
						});
					} else {
						scope.vehicle.id = null;
						scope.vehicle.make = null;
						scope.vehicle.model = null;
						scope.vehicle.year = null;
						scope.vehicle.transmission = null;
						scope.vehicle.vehicleTypeId = null;
						//retain existing VIN
						//retain existing color
						//retain existing licenseNum
					};
				})
			}
		}
	})
	.directive('selectReqSite', function() {
		return {
			restrict: 'A',
			link: function(scope) {
				scope.statusEntry.reqEmpDutyId = scope.reqEmpDuty.id;
			}
		}
	})
	// .directive('ngFileSelect', [ '$fileUploader', function ($fileUploader) {
	//     return {
	//     	restrict: 'A',
	//         link: function (scope, element, attributes) {
	//             $fileUploader.isHTML5 || element.removeAttr('multiple');

	//             element.bind('change', function () {
	//                 scope.$emit('file:add', $fileUploader.isHTML5 ? this.files : this, scope.$eval(attributes.ngFileSelect));
	//                 ($fileUploader.isHTML5 && element.attr('multiple')) && element.prop('value', null);
	//             });

	//             element.prop('value', null); // FF fix
	//         }
	//     };
	// }])