angular.module('controllers', ['services'])
	.controller('loginCtrl', function($scope, $location, AuthenService, FlashService) {
		$scope.credentials = {username: "", password: ""};

		$scope.login = function() {
			AuthenService.login($scope.credentials).$promise
				.then(function(data) {
					FlashService.show("Welcome, " +data.user.firstName +" " +data.user.lastName);
					AuthenService.setCurrentUser(data.user);
					switch ($scope.userType) {
						case "towEmp":
							$location.path('/tow-home');
							break;
						case "reqEmp":
							$location.path('/req-home');
							break;
						default:
							console.log("You're NOT a valid user!"); //NEED TO THROW AN ERROR
					}

				}, function(err) {
					console.log("Login Failed: " +err);
				});
		}
	})
	.controller('towHomeCtrl', function($scope, $route, TowSitesDataService, TowReqsDataService, StatusesDataService, VehiclesDataService, TowEmpDutiesDataService) {
		//var self = this;
		$scope.towSites = [];
		$scope.statusEntries = [];
		$scope.statuses = [];
		$scope.vehicleTypes = [];
		$scope.towReqs = [];
		$scope.showMapCanvas = true;
		$scope.showTowReqDetails = false;
		$scope.chosenTowReq;
		$scope.currentUserId = localStorage.$LoopBack$currentUserId;

		initialize = function(towEmpId) {
			var towSiteIds = [];

			TowSitesDataService.fetchTowSiteIds(towEmpId)
				.then(function(results) {
					if (results.length > 0) {
						$scope.towSites = TowSitesDataService.getTowSites();
						return towSiteIds = results;
					}
						
				}, function(errData) {
					console.log("Couldn't fetch TOW SITES IDs: ", errResults);
				})
				.then(function(towSiteIds) {
					return TowReqsDataService.fetchTowReqs(towSiteIds)
						.then(function(results) {
							return $scope.towReqs = results;

						}, function(errData) {
							console.log("Couldn't fetch TOW REQs ", errResults);
						})
				})
				.then(function(towReqs) {
					StatusesDataService.setStatusEntries(towReqs);
					$scope.statusEntries = StatusesDataService.getStatusEntries();
				})
					
				StatusesDataService.setStatuses($route.current.locals.statuses);
				$scope.statuses = $route.current.locals.statuses

				VehiclesDataService.setVehicleTypes($route.current.locals.vehicleTypes);
				$scope.vehicleTypes = $route.current.locals.vehicleTypes;

				TowEmpDutiesDataService.fetchTowEmpDuties(parseInt($scope.currentUserId));
		}

		$scope.setStatuses = function() {
			DataService.setStatuses().then(function(results) {
				$scope.statuses = results
			})
		}

		$scope.setVehicleTypes = function() {
			DataService.setVehicleTypes().then(function(results) {
				$scope.vehicleTypes = results;
			})
		},

		initialize($scope.currentUserId);
	})
	.controller('reqHomeCtrl', function($rootScope, $scope, $route) {	
		$scope.currentUserId = localStorage.$LoopBack$currentUserId;	
		$scope.viewsToShow = {'sites': false, 'emps': false, 'tow-reqs': true};

		initialize = function(towEmpId) {
			
		}

		$scope.testAPIData = function() {
			APITestService.getTowOrgs();
		}

		initialize($scope.currentUserId);
	})
	.controller('towReqsViewCtrl', function($scope, $fileUploader, VehiclesDataService, ReqSitesDataService, TowReqsDataService, LocationsDataService, StatusesDataService, DateTimeService) {
		'use strict';
		$scope.towReq;
		$scope.vehicle = {vin: ""};
		$scope.detailsMode = true;
		$scope.cameraMode = true;
		$scope.takenPicture = {};
		$scope.statusEntry = {};
		$scope.reqPictures = [];
		$scope.reqEmpDuties = [];
		$scope.pictureTypes = [];
		$scope.vehicleTypes = [];
		$scope.input = {
			vin: true,
			licNo: true,
			vehicle: true
		};

		VehiclesDataService.fetchVehicleTypes()
			.then(function(results) {
				$scope.vehicleTypes = results;
			})

		ReqSitesDataService.fetchReqSites($scope.currentUserId)
			.then(function(results) {
				$scope.reqEmpDuties = results;
			}, function(err) {
				console.log("Can't fetch ReqSites")
			})

		TowReqsDataService.fetchPictureTypes()
			.then(function(results) {
				for (var cnt = 0; cnt < results.length; cnt++) {
					results[cnt].state = false;
				}
				
				$scope.pictureTypes = results;

			}, function(error) {
				console.log("Couldn't attempt to retrieve pictureTypes");
			});

		$scope.getVehicleData = function(vin, callback) {
			VehiclesDataService.getIntVehicleData(vin)
				.then(function(intVehicleData) { 	// RESULTS > 0
					if(intVehicleData[0] != undefined) {
						callback(intVehicleData[0]);
					} else {
						VehiclesDataService.getExtVehicleData(vin)
							.then(function(extVehicleData) {
								callback(extVehicleData);
							}, function(extVehicleError) {
								console.log("Counldn't look for Vehicle Info: ", extVehicleError.message);
							})
					}
				
				}, function(intVehicleError) { // ERROR IN TRYING TO GET DATA
					console.log("Couldn't Connect to towBYTE DB: ", intVehicleError.data);
				});
		}

		$scope.toggleMode = function(callingEl) {
			$scope.removePicture();
			$scope.takenPicture = {};
			$scope.detailsMode = !$scope.detailsMode;
			$scope.callingEl = callingEl;
		}

		$scope.takePicture = function() {
			$scope.cameraMode = false;

			//get GeoLocation Info, and add to $scope.takenPic
			LocationsDataService.getCurrentLocation()
				.then(function(results) {
					$scope.reqPictures.geoLocation = results.coords;
					$scope.reqPictures.timestamp = $scope.takenPicture.timestamp = DateTimeService.now();
					//Q:  how to ensure timestamp is recorded for current picture before next picture is taken.
					console.log("TIMESTAMP: ", $scope.takenPicture.timestamp);
				}, function(err) {
					console.log("Problem getting current loc, ", err.data);
				})
	    	
	    	//get pictureTypeId of taken picture	
	    	$scope.takenPicture.pictureTypeId = $scope.callingEl.children[0].attributes[0].value;
		}

		$scope.keepPicture = function() {
			$scope.cameraMode = false;
		}

		$scope.removePicture = function() {
			$scope.cameraMode = true;
		}

		$scope.toggleInput = function(param) {
			$scope.input.vin = !$scope.input.vin;
			$scope.input.licNo = !$scope.input.licNo;
			$scope.input.vehicle = !$scope.input.vehicle;
			$scope.input[param] = true;
			$scope.toggleMode();
		}

		$scope.stageReqPicture = function(takenPicture) {
			// $scope.reqPictures[$scope.takenPicture.pictureTypeId] = $scope.takenPicture;
			$scope.reqPictures[$scope.takenPicture.pictureTypeId] = takenPicture;
		}

		$scope.initTowReq = function() {

			/* 1 - Check reqSites array for allowed Requestor Site
			UPDATE:  DON'T CHECK REQSITES -- ALREADY ASSUMED THAT WEB-SOCKETING WILL TAKE CARE OF PERMISSIONS
			*/
			// if ( !_.findWhere($scope.reqSites, {reqSiteId: $scope.towReq.reqSiteId}) );	

			/* 2b - create vehicle and GET vehicle Id */
			if ($scope.vehicle.id === null || $scope.vehicle.id === undefined) {
				console.log("#2a - Creating vehicle: ", $scope.vehicle.make +" " +$scope.vehicle.model);
				VehiclesDataService.createVehicle($scope.vehicle)
					.then(function(createResults) {
						console.log($scope.vehicle.make +" " +$scope.vehicle.model +" created!");
						$scope.towReq.vehicleId = $scope.vehicle.id = createResults.id;
						console.log("Vehicle Id is:  ", $scope.towReq.vehicleId);
						// Q: What if the vehicle happens to have been created just before creating the vehicle?

						return createResults.id;
					}, function(createErr) {
						// Connection problem
						console.log("Having problems creating Vehicle: ", createErr.data);
					})
					.then(function(vehicleId) {
						$scope.createTowReq(vehicleId);
					})
			} else {
				console.log("#2b - Vehicle already exists...");
				$scope.towReq.vehicleId = $scope.vehicle.id;
				$scope.createTowReq($scope.vehicle.id);
			}
		}

		$scope.createTowReq = function(vehicleId) {
			/* 3 - create TowReq */
			console.log("#3 - Creating TowReq...");
			TowReqsDataService.createTowReq($scope.towReq)
				.then(function(towReqResults) {
					console.log("TowReq created!");
					return ($scope.towReq.id = towReqResults.id);
				}, function(createErr) {
					console.log("Can't create TowReq: ", createErr.data);
				})
				.then(function(towReqId) {
					// Stage Status Entry information
					$scope.statusEntry.towEmpDutyId = 1;
					$scope.statusEntry.towReqId = towReqId;
					$scope.statusEntry.statusId = 2;
					$scope.statusEntry.timestamp = DateTimeService.now();
					$scope.statusEntry.lat = $scope.reqPictures.geoLocation.latitude;
					$scope.statusEntry.long = $scope.reqPictures.geoLocation.longitude;

					console.log("Status Entry: ", $scope.statusEntry)
					console.log("#4a - Creating Status Entry...");

					for (var i=0; i < $scope.reqEmpDuties.length; i++) {
						if ( ($scope.reqEmpDuties[i].reqSiteId === $scope.towReq.reqSiteId)
							&& ($scope.reqEmpDuties[i].reqDutyId === 1)
							&& ($scope.reqEmpDuties[i].isActive === 1) ) {
							statusEntry.reqEmpDutyId = $scope.reqEmpDuties[i].id;	
						}
					}

					// #4a - Create StatusEntry
					StatusesDataService.createStatusEntry($scope.statusEntry)
						.then(function(status) {
							console.log("Status Created!");
						}, function(errResults) {
							console.log("Can't create status: ", errResults);
						});

					return $scope.towReq.id;
				})
				.then(function(towReqId) {
					$scope.reqPictures.towReqId = towReqId;
					$scope.reqPictures = TowReqsDataService.enrichReqPictures($scope.reqPictures);
					
					//#4b - Create ReqPictures
					console.log("Req Pictures: ", $scope.reqPictures);

					_.each($scope.reqPictures, function(reqPicture) {
						if (reqPicture) {
							TowReqsDataService.addReqPicture(reqPicture)
								.then(function(addResult) {
									console.log(addResult, " added.")
								}, function(addError) {
									console.log("Couldn't add picture: ", addError);
								});
						}
					});
				});
		}

	    // create a uploader with options
	    var uploader = $scope.uploader = $fileUploader.create({
	      scope: $scope,                          // to automatically update the html. Default: $rootScope
	      url: '/api/containers/temp/upload',
	      formData: [
	        { key: 'value' }
	      ],
	      filters: [
	        function (item) {                    // first user filter
	          console.info('filter1');
	          return true;
	        }
	      ]
	    });

	    // ADDING FILTERS
	    uploader.filters.push(function (item) { // second user filter
	      console.info('filter2');
	      return true;
	    });

	    // REGISTER HANDLERS
	    uploader.bind('afteraddingfile', function (event, item) {
	      console.info('After adding a file', item);
	    });

	    uploader.bind('whenaddingfilefailed', function (event, item) {
	      console.info('When adding a file failed', item);
	    });

	    uploader.bind('afteraddingall', function (event, items) {
	      console.info('After adding all files', items);
	    });

	    uploader.bind('beforeupload', function (event, item) {
	      console.info('Before upload', item);
	    });

	    uploader.bind('progress', function (event, item, progress) {
	      console.info('Progress: ' + progress, item);
	    });

	    uploader.bind('success', function (event, xhr, item, response) {
	      console.info('Success', xhr, item, response);
	      $scope.$broadcast('uploadCompleted', item);
	    });

	    uploader.bind('cancel', function (event, xhr, item) {
	      console.info('Cancel', xhr, item);
	    });

	    uploader.bind('error', function (event, xhr, item, response) {
	      console.info('Error', xhr, item, response);
	    });

	    uploader.bind('complete', function (event, xhr, item, response) {
	      console.info('Complete', xhr, item, response);
	    });

	    uploader.bind('progressall', function (event, progress) {
	      console.info('Total progress: ' + progress);
	    });

	    uploader.bind('completeall', function (event, items) {
	      console.info('Complete all', items);
	    });
	})
	.controller('authenCtrl', function($scope, AuthenService, APITestService) {
		$scope.logout = function() {
			console.log("logging out...");
			AuthenService.logout();
		};

		$scope.testAPIData = function() {
			APITestService.getTowOrgs();
		};
	});