angular.module('filters', ['services'])
	.filter('statusMapping', function(StatusesDataService) {
		return function(statusId) {
			return StatusesDataService.getStatus(statusId).name
		}
	})
	.filter('vehicleTypeMapping', function(VehiclesDataService) {
		return function(vehicleTypeId) {
			return VehiclesDataService.getVehicleType(vehicleTypeId).name
		}
	})
    .filter('property', function() {		 
	    function parseString(input){
	        return input.split("."); //return array
	    }
	 
	 	function getArrayElement(arrayRule, lastArrayElement) {
	    	switch(arrayRule) {
	    		case 'first':
	    			return 0;
	    		case 'last':
	    			return lastArrayElement;
	    	}
	    }

	    function getValue(element, propertyArray, arrayRule){
	        var value = element;
	 
	        _.forEach(propertyArray, function(property){
	            if (value[property] instanceof Array) {
	            	value = value[property][getArrayElement(arrayRule, value[property].length-1)];
	            } else {
	            	value = value[property];
	            }
	        });
	 
	        return value;
	    }
	 
	    return function (object, propertyString, arrayRule, target){
	        var properties = parseString(propertyString);

	        return _.filter(object, function(item){
	            // console.log("-------------------------------------------------");
	            // console.log("STATUS ID: ", getValue(item, properties, arrayRule));
	            // console.log("target ", target);
	            // console.log(getValue(item, properties, arrayRule) == target);
	            // if (!target) {
	            // 	return true;
	            // }
	            	
	            return (getValue(item, properties, arrayRule) == target || !target )? true : false
	        });
	    }
    })