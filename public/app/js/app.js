var app = angular.module('app', ['ngRoute', 'controllers', 'directives', 'services', 'filters'])
	.config(function($routeProvider) {
		$routeProvider
			.when("/login", {
				templateUrl: 'login.html',
				controller: 'loginCtrl'
			})
			.when("/tow-home", {
				templateUrl: 'tow-home.html',
				controller: 'towHomeCtrl',
				resolve: {						
					statuses: function($route, StatusesDataService) {
						return StatusesDataService.fetchStatuses();
					},

					vehicleTypes: function($route, VehiclesDataService) {
						return VehiclesDataService.fetchVehicleTypes();
					}	
				}
			})
			.when("/req-home", {
				templateUrl: 'req-home.html',
				controller: 'reqHomeCtrl'
			})
			.otherwise({
				redirectTo: "/login"
			});
	})
	.run(function($location, $rootScope, $templateCache, $http, AuthenService, FlashService) {
		var routesThatRequireAuth = ['/tow-home', '/req-home'];
		$http.get("emps-view.html", { cache: $templateCache});
		$http.get("sites-view.html", { cache: $templateCache});
		$http.get("tow-reqs-view.html", { cache: $templateCache});

		$rootScope.$on('$routeChangeStart', function(event, next, current) {
			if (_(routesThatRequireAuth).contains($location.path()) && !AuthenService.isLoggedIn()) {
				FlashService.show("Please login before seeing other pages...");
				$location.path("/login");
			}
		});
	})

//build run function (validate user is logged in)