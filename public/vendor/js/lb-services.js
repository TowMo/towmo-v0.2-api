(function(window, angular, undefined) {'use strict';

var urlBase = "/api";

/**
 * @ngdoc overview
 * @name lbServices
 * @module
 * @description
 *
 * The `lbServices` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
var module = angular.module("lbServices", ['ngResource']);

/**
 * @ngdoc object
 * @name lbServices.User
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `User` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "User",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/users/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.User#login
         * @methodOf lbServices.User
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
         *
         *  - `rememberMe` - `boolean` - Whether the authentication credentials
         *     should be remembered in localStorage across app/browser restarts.
         *     Default: `true`.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * The response body contains properties of the AccessToken created on login.
         * Depending on the value of `include` parameter, the body may contain additional properties:
         * 
         *   - `user` - `{User}` - Data of the currently logged in user. (`include=user`)
         * 
         *
         */
        "login": {
          url: urlBase + "/users/login",
          method: "POST",
          interceptor: {
            response: function(response) {
              var accessToken = response.data;
              LoopBackAuth.currentUserId = accessToken.userId;
              LoopBackAuth.accessTokenId = accessToken.id;
              LoopBackAuth.rememberMe = response.config.params.rememberMe !== false;
              LoopBackAuth.save();
              return response.resource;
            }
          }
        },

        /**
         * @ngdoc method
         * @name lbServices.User#logout
         * @methodOf lbServices.User
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `access_token` – `{string}` - Do not supply this argument, it is automatically extracted from request headers.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "logout": {
          url: urlBase + "/users/logout",
          method: "POST",
          interceptor: {
            response: function(response) {
              LoopBackAuth.currentUserId = null;
              LoopBackAuth.accessTokenId = null;
              LoopBackAuth.save();
              return response.resource;
            }
          }
        },

        /**
         * @ngdoc method
         * @name lbServices.User#confirm
         * @methodOf lbServices.User
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `uid` – `{string}` - 
         *
         *  - `token` – `{string}` - 
         *
         *  - `redirect` – `{string}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "confirm": {
          url: urlBase + "/users/confirm",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#resetPassword
         * @methodOf lbServices.User
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "resetPassword": {
          url: urlBase + "/users/reset",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#email
         * @methodOf lbServices.User
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method does not accept any data. Supply an empty object.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "email": {
          url: urlBase + "/users/Emails",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#accessToken
         * @methodOf lbServices.User
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method does not accept any data. Supply an empty object.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "accessToken": {
          url: urlBase + "/users/AccessTokens",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#create
         * @methodOf lbServices.User
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/users",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#updateOrCreate
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/users",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#upsert
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/users",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#exists
         * @methodOf lbServices.User
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/users/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#findById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/users/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#find
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/users",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.User#findOne
         * @methodOf lbServices.User
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/users/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#destroyById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/users/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#deleteById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/users/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#removeById
         * @methodOf lbServices.User
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/users/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#count
         * @methodOf lbServices.User
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/users/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$updateAttributes
         * @methodOf lbServices.User
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/users/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__get__accessTokens
         * @methodOf lbServices.User
         * @deprecated Use user.accessTokens() instead.
         *
         * @description
         *
         * Queries accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__get__accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__create__accessTokens
         * @methodOf lbServices.User
         * @deprecated Use user.accessTokens.create() instead.
         *
         * @description
         *
         * Creates a new instance in accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__create__accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#prototype$__delete__accessTokens
         * @methodOf lbServices.User
         * @deprecated Use user.accessTokens.destroyAll() instead.
         *
         * @description
         *
         * Deletes all accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        "prototype$__delete__accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "DELETE",
        },

        // INTERNAL. Use accessToken.user() instead.
        "::get::accessToken::user": {
          url: urlBase + "/accessTokens/:id/user",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.User#getCurrent
         * @methodOf lbServices.User
         *
         * @description
         *
         * Get data of the currently logged user. Fail with HTTP result 401
         * when there is no user logged in.
         *
         * @param {Function(Object, Object)=} successCb
         *    Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *    `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         */
        "getCurrent": {
          url: urlBase + "/users/:id",
          method: "GET",
          params: {
            id: function() {
             var id = LoopBackAuth.currentUserId;
             if (id == null) id = '__anonymous__';
             return id;
           }
          },
          __isGetCurrentUser__ : true
        }
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.User#accessTokens
         * @methodOf lbServices.User
         * @deprecated Use user.accessTokens() instead.
         *
         * @description
         *
         * Queries accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        R.accessTokens = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::get::user::accessTokens"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.User#accessTokens.create
         * @methodOf lbServices.User
         * @deprecated Use user.accessTokens.create() instead.
         *
         * @description
         *
         * Creates a new instance in accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        R.accessTokens.create = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::create::user::accessTokens"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.User#accessTokens.destroyAll
         * @methodOf lbServices.User
         * @deprecated Use user.accessTokens.destroyAll() instead.
         *
         * @description
         *
         * Deletes all accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        R.accessTokens.destroyAll = function() {
          var TargetResource = $injector.get("AccessToken");
          var action = TargetResource["::delete::user::accessTokens"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.AccessToken
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `AccessToken` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "AccessToken",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/accessTokens/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#create
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/accessTokens",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#updateOrCreate
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/accessTokens",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#upsert
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/accessTokens",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#exists
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/accessTokens/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#findById
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/accessTokens/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#find
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/accessTokens",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#findOne
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/accessTokens/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#destroyById
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/accessTokens/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#deleteById
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/accessTokens/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#removeById
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/accessTokens/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#count
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/accessTokens/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#prototype$updateAttributes
         * @methodOf lbServices.AccessToken
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/accessTokens/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.AccessToken#prototype$__get__user
         * @methodOf lbServices.AccessToken
         * @deprecated Use accessToken.user() instead.
         *
         * @description
         *
         * Fetches belongsTo relation user
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `AccessToken` object.)
         * </em>
         */
        "prototype$__get__user": {
          url: urlBase + "/accessTokens/:id/user",
          method: "GET",
        },

        // INTERNAL. Use user.accessTokens() instead.
        "::get::user::accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use user.accessTokens.create() instead.
        "::create::user::accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "POST",
        },

        // INTERNAL. Use user.accessTokens.destroyAll() instead.
        "::delete::user::accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "DELETE",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.AccessToken#user
         * @methodOf lbServices.AccessToken
         * @deprecated Use accessToken.user() instead.
         *
         * @description
         *
         * Fetches belongsTo relation user
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `User` object.)
         * </em>
         */
        R.user = function() {
          var TargetResource = $injector.get("User");
          var action = TargetResource["::get::accessToken::user"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Application
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Application` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Application",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/applications/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Application#create
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Application` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/applications",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#updateOrCreate
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Application` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/applications",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#upsert
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Application` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/applications",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#exists
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/applications/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#findById
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Application` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/applications/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#find
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Application` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/applications",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#findOne
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Application` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/applications/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#destroyById
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/applications/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#deleteById
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/applications/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#removeById
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/applications/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#count
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/applications/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Application#prototype$updateAttributes
         * @methodOf lbServices.Application
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Application` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/applications/:id",
          method: "PUT",
        },
      }
    );


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Push
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Push` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Push",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/pushes/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Push#notifyByQuery
         * @methodOf lbServices.Push
         *
         * @description
         *
         * Send a push notification by installation query
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `deviceQuery` – `{object=}` - Installation query
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Push` object.)
         * </em>
         */
        "notifyByQuery": {
          url: urlBase + "/pushes",
          method: "POST",
        },
      }
    );


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Installation
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Installation` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Installation",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/installations/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Installation#findByApp
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Find installations by application id
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `deviceType` – `{string=}` - Device type
         *
         *  - `appId` – `{string=}` - Application id
         *
         *  - `appVersion` – `{string=}` - Application version
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "findByApp": {
          url: urlBase + "/installations/byApp",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#findByUser
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Find installations by user id
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `deviceType` – `{string=}` - Device type
         *
         *  - `userId` – `{string=}` - User id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "findByUser": {
          url: urlBase + "/installations/byUser",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#findBySubscriptions
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Find installations by subscriptions
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `deviceType` – `{string=}` - Device type
         *
         *  - `subscriptions` – `{string=}` - Subscriptions
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "findBySubscriptions": {
          url: urlBase + "/installations/bySubscriptions",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#create
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/installations",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#updateOrCreate
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/installations",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#upsert
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/installations",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#exists
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/installations/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#findById
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/installations/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#find
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/installations",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#findOne
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/installations/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#destroyById
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/installations/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#deleteById
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/installations/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#removeById
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/installations/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#count
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/installations/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Installation#prototype$updateAttributes
         * @methodOf lbServices.Installation
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Installation` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/installations/:id",
          method: "PUT",
        },
      }
    );


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Notification
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Notification` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Notification",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/notifications/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Notification#create
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Notification` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/notifications",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#updateOrCreate
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Notification` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/notifications",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#upsert
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Notification` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/notifications",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#exists
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/notifications/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#findById
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Notification` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/notifications/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#find
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Notification` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/notifications",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#findOne
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Notification` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/notifications/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#destroyById
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/notifications/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#deleteById
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/notifications/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#removeById
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/notifications/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#count
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/notifications/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Notification#prototype$updateAttributes
         * @methodOf lbServices.Notification
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Notification` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/notifications/:id",
          method: "PUT",
        },
      }
    );


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.State
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `State` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "State",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/states/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.State#create
         * @methodOf lbServices.State
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/states",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#updateOrCreate
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/states",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#upsert
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/states",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#exists
         * @methodOf lbServices.State
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/states/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#findById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/states/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#find
         * @methodOf lbServices.State
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/states",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.State#findOne
         * @methodOf lbServices.State
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/states/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#destroyById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/states/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#deleteById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/states/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#removeById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/states/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#count
         * @methodOf lbServices.State
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/states/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#prototype$updateAttributes
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/states/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#prototype$__get__towSites
         * @methodOf lbServices.State
         * @deprecated Use state.towSites() instead.
         *
         * @description
         *
         * Queries towSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "prototype$__get__towSites": {
          url: urlBase + "/states/:id/towSites",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.State#prototype$__create__towSites
         * @methodOf lbServices.State
         * @deprecated Use state.towSites.create() instead.
         *
         * @description
         *
         * Creates a new instance in towSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "prototype$__create__towSites": {
          url: urlBase + "/states/:id/towSites",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#prototype$__delete__towSites
         * @methodOf lbServices.State
         * @deprecated Use state.towSites.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "prototype$__delete__towSites": {
          url: urlBase + "/states/:id/towSites",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#prototype$__get__reqSites
         * @methodOf lbServices.State
         * @deprecated Use state.reqSites() instead.
         *
         * @description
         *
         * Queries reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "prototype$__get__reqSites": {
          url: urlBase + "/states/:id/reqSites",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.State#prototype$__create__reqSites
         * @methodOf lbServices.State
         * @deprecated Use state.reqSites.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "prototype$__create__reqSites": {
          url: urlBase + "/states/:id/reqSites",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.State#prototype$__delete__reqSites
         * @methodOf lbServices.State
         * @deprecated Use state.reqSites.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "prototype$__delete__reqSites": {
          url: urlBase + "/states/:id/reqSites",
          method: "DELETE",
        },

        // INTERNAL. Use towSite.state() instead.
        "::get::towSite::state": {
          url: urlBase + "/towSites/:id/state",
          method: "GET",
        },

        // INTERNAL. Use reqSite.state() instead.
        "::get::reqSite::state": {
          url: urlBase + "/reqSites/:id/state",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.State#towSites
         * @methodOf lbServices.State
         * @deprecated Use state.towSites() instead.
         *
         * @description
         *
         * Queries towSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        R.towSites = function() {
          var TargetResource = $injector.get("TowSite");
          var action = TargetResource["::get::state::towSites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State#towSites.create
         * @methodOf lbServices.State
         * @deprecated Use state.towSites.create() instead.
         *
         * @description
         *
         * Creates a new instance in towSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        R.towSites.create = function() {
          var TargetResource = $injector.get("TowSite");
          var action = TargetResource["::create::state::towSites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State#towSites.destroyAll
         * @methodOf lbServices.State
         * @deprecated Use state.towSites.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        R.towSites.destroyAll = function() {
          var TargetResource = $injector.get("TowSite");
          var action = TargetResource["::delete::state::towSites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State#reqSites
         * @methodOf lbServices.State
         * @deprecated Use state.reqSites() instead.
         *
         * @description
         *
         * Queries reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSites = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::get::state::reqSites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State#reqSites.create
         * @methodOf lbServices.State
         * @deprecated Use state.reqSites.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSites.create = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::create::state::reqSites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.State#reqSites.destroyAll
         * @methodOf lbServices.State
         * @deprecated Use state.reqSites.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSites.destroyAll = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::delete::state::reqSites"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TowSite
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TowSite` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TowSite",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/towSites/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.TowSite#create
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/towSites",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#updateOrCreate
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/towSites",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#upsert
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/towSites",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#exists
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/towSites/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#findById
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/towSites/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#find
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/towSites",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#findOne
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/towSites/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#destroyById
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/towSites/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#deleteById
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/towSites/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#removeById
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/towSites/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#count
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/towSites/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$updateAttributes
         * @methodOf lbServices.TowSite
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/towSites/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__get__state
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.state() instead.
         *
         * @description
         *
         * Fetches belongsTo relation state
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__get__state": {
          url: urlBase + "/towSites/:id/state",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__get__towSiteGeos
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towSiteGeos() instead.
         *
         * @description
         *
         * Queries towSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__get__towSiteGeos": {
          url: urlBase + "/towSites/:id/towSiteGeos",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__create__towSiteGeos
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towSiteGeos.create() instead.
         *
         * @description
         *
         * Creates a new instance in towSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__create__towSiteGeos": {
          url: urlBase + "/towSites/:id/towSiteGeos",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__delete__towSiteGeos
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towSiteGeos.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__delete__towSiteGeos": {
          url: urlBase + "/towSites/:id/towSiteGeos",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__get__towVehicles
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towVehicles() instead.
         *
         * @description
         *
         * Queries towVehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__get__towVehicles": {
          url: urlBase + "/towSites/:id/towVehicles",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__create__towVehicles
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towVehicles.create() instead.
         *
         * @description
         *
         * Creates a new instance in towVehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__create__towVehicles": {
          url: urlBase + "/towSites/:id/towVehicles",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__delete__towVehicles
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towVehicles.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towVehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__delete__towVehicles": {
          url: urlBase + "/towSites/:id/towVehicles",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__get__towEmpDuties
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towEmpDuties() instead.
         *
         * @description
         *
         * Queries towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__get__towEmpDuties": {
          url: urlBase + "/towSites/:id/towEmpDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__create__towEmpDuties
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__create__towEmpDuties": {
          url: urlBase + "/towSites/:id/towEmpDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__delete__towEmpDuties
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__delete__towEmpDuties": {
          url: urlBase + "/towSites/:id/towEmpDuties",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__get__reqSites
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.reqSites() instead.
         *
         * @description
         *
         * Queries reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__get__reqSites": {
          url: urlBase + "/towSites/:id/reqSites",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__create__reqSites
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.reqSites.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__create__reqSites": {
          url: urlBase + "/towSites/:id/reqSites",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSite#prototype$__delete__reqSites
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.reqSites.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        "prototype$__delete__reqSites": {
          url: urlBase + "/towSites/:id/reqSites",
          method: "DELETE",
        },

        // INTERNAL. Use state.towSites() instead.
        "::get::state::towSites": {
          url: urlBase + "/states/:id/towSites",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use state.towSites.create() instead.
        "::create::state::towSites": {
          url: urlBase + "/states/:id/towSites",
          method: "POST",
        },

        // INTERNAL. Use state.towSites.destroyAll() instead.
        "::delete::state::towSites": {
          url: urlBase + "/states/:id/towSites",
          method: "DELETE",
        },

        // INTERNAL. Use towSiteGeo.towSite() instead.
        "::get::towSiteGeo::towSite": {
          url: urlBase + "/towSiteGeos/:id/towSite",
          method: "GET",
        },

        // INTERNAL. Use towVehicle.towSite() instead.
        "::get::towVehicle::towSite": {
          url: urlBase + "/towVehicles/:id/towSite",
          method: "GET",
        },

        // INTERNAL. Use towEmpDuty.towSite() instead.
        "::get::towEmpDuty::towSite": {
          url: urlBase + "/towEmpDuties/:id/towSite",
          method: "GET",
        },

        // INTERNAL. Use reqSite.towSite() instead.
        "::get::reqSite::towSite": {
          url: urlBase + "/reqSites/:id/towSite",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.TowSite#state
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.state() instead.
         *
         * @description
         *
         * Fetches belongsTo relation state
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        R.state = function() {
          var TargetResource = $injector.get("State");
          var action = TargetResource["::get::towSite::state"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#towSiteGeos
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towSiteGeos() instead.
         *
         * @description
         *
         * Queries towSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        R.towSiteGeos = function() {
          var TargetResource = $injector.get("TowSiteGeo");
          var action = TargetResource["::get::towSite::towSiteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#towSiteGeos.create
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towSiteGeos.create() instead.
         *
         * @description
         *
         * Creates a new instance in towSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        R.towSiteGeos.create = function() {
          var TargetResource = $injector.get("TowSiteGeo");
          var action = TargetResource["::create::towSite::towSiteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#towSiteGeos.destroyAll
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towSiteGeos.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        R.towSiteGeos.destroyAll = function() {
          var TargetResource = $injector.get("TowSiteGeo");
          var action = TargetResource["::delete::towSite::towSiteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#towVehicles
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towVehicles() instead.
         *
         * @description
         *
         * Queries towVehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        R.towVehicles = function() {
          var TargetResource = $injector.get("TowVehicle");
          var action = TargetResource["::get::towSite::towVehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#towVehicles.create
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towVehicles.create() instead.
         *
         * @description
         *
         * Creates a new instance in towVehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        R.towVehicles.create = function() {
          var TargetResource = $injector.get("TowVehicle");
          var action = TargetResource["::create::towSite::towVehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#towVehicles.destroyAll
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towVehicles.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towVehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        R.towVehicles.destroyAll = function() {
          var TargetResource = $injector.get("TowVehicle");
          var action = TargetResource["::delete::towSite::towVehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#towEmpDuties
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towEmpDuties() instead.
         *
         * @description
         *
         * Queries towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuties = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::get::towSite::towEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#towEmpDuties.create
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuties.create = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::create::towSite::towEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#towEmpDuties.destroyAll
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.towEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuties.destroyAll = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::delete::towSite::towEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#reqSites
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.reqSites() instead.
         *
         * @description
         *
         * Queries reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSites = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::get::towSite::reqSites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#reqSites.create
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.reqSites.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSites.create = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::create::towSite::reqSites"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowSite#reqSites.destroyAll
         * @methodOf lbServices.TowSite
         * @deprecated Use towSite.reqSites.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqSites of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSites.destroyAll = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::delete::towSite::reqSites"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TowSiteGeo
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TowSiteGeo` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TowSiteGeo",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/towSiteGeos/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#create
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/towSiteGeos",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#updateOrCreate
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/towSiteGeos",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#upsert
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/towSiteGeos",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#exists
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/towSiteGeos/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#findById
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/towSiteGeos/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#find
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/towSiteGeos",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#findOne
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/towSiteGeos/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#destroyById
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/towSiteGeos/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#deleteById
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/towSiteGeos/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#removeById
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/towSiteGeos/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#count
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/towSiteGeos/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#prototype$updateAttributes
         * @methodOf lbServices.TowSiteGeo
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/towSiteGeos/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#prototype$__get__towSite
         * @methodOf lbServices.TowSiteGeo
         * @deprecated Use towSiteGeo.towSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSiteGeo` object.)
         * </em>
         */
        "prototype$__get__towSite": {
          url: urlBase + "/towSiteGeos/:id/towSite",
          method: "GET",
        },

        // INTERNAL. Use towSite.towSiteGeos() instead.
        "::get::towSite::towSiteGeos": {
          url: urlBase + "/towSites/:id/towSiteGeos",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use towSite.towSiteGeos.create() instead.
        "::create::towSite::towSiteGeos": {
          url: urlBase + "/towSites/:id/towSiteGeos",
          method: "POST",
        },

        // INTERNAL. Use towSite.towSiteGeos.destroyAll() instead.
        "::delete::towSite::towSiteGeos": {
          url: urlBase + "/towSites/:id/towSiteGeos",
          method: "DELETE",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.TowSiteGeo#towSite
         * @methodOf lbServices.TowSiteGeo
         * @deprecated Use towSiteGeo.towSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        R.towSite = function() {
          var TargetResource = $injector.get("TowSite");
          var action = TargetResource["::get::towSiteGeo::towSite"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TowVehicle
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TowVehicle` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TowVehicle",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/towVehicles/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#create
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/towVehicles",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#updateOrCreate
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/towVehicles",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#upsert
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/towVehicles",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#exists
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/towVehicles/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#findById
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/towVehicles/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#find
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/towVehicles",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#findOne
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/towVehicles/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#destroyById
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/towVehicles/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#deleteById
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/towVehicles/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#removeById
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/towVehicles/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#count
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/towVehicles/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#prototype$updateAttributes
         * @methodOf lbServices.TowVehicle
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/towVehicles/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#prototype$__get__towSite
         * @methodOf lbServices.TowVehicle
         * @deprecated Use towVehicle.towSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowVehicle` object.)
         * </em>
         */
        "prototype$__get__towSite": {
          url: urlBase + "/towVehicles/:id/towSite",
          method: "GET",
        },

        // INTERNAL. Use towSite.towVehicles() instead.
        "::get::towSite::towVehicles": {
          url: urlBase + "/towSites/:id/towVehicles",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use towSite.towVehicles.create() instead.
        "::create::towSite::towVehicles": {
          url: urlBase + "/towSites/:id/towVehicles",
          method: "POST",
        },

        // INTERNAL. Use towSite.towVehicles.destroyAll() instead.
        "::delete::towSite::towVehicles": {
          url: urlBase + "/towSites/:id/towVehicles",
          method: "DELETE",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.TowVehicle#towSite
         * @methodOf lbServices.TowVehicle
         * @deprecated Use towVehicle.towSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        R.towSite = function() {
          var TargetResource = $injector.get("TowSite");
          var action = TargetResource["::get::towVehicle::towSite"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TowEmp
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TowEmp` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TowEmp",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/towEmps/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#create
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/towEmps",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#updateOrCreate
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/towEmps",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#upsert
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/towEmps",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#exists
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/towEmps/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#findById
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/towEmps/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#find
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/towEmps",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#findOne
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/towEmps/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#destroyById
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/towEmps/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#deleteById
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/towEmps/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#removeById
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/towEmps/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#count
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/towEmps/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#prototype$updateAttributes
         * @methodOf lbServices.TowEmp
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/towEmps/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#prototype$__get__towEmpDuties
         * @methodOf lbServices.TowEmp
         * @deprecated Use towEmp.towEmpDuties() instead.
         *
         * @description
         *
         * Queries towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "prototype$__get__towEmpDuties": {
          url: urlBase + "/towEmps/:id/towEmpDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#prototype$__create__towEmpDuties
         * @methodOf lbServices.TowEmp
         * @deprecated Use towEmp.towEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "prototype$__create__towEmpDuties": {
          url: urlBase + "/towEmps/:id/towEmpDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#prototype$__delete__towEmpDuties
         * @methodOf lbServices.TowEmp
         * @deprecated Use towEmp.towEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        "prototype$__delete__towEmpDuties": {
          url: urlBase + "/towEmps/:id/towEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use towEmpDuty.towEmp() instead.
        "::get::towEmpDuty::towEmp": {
          url: urlBase + "/towEmpDuties/:id/towEmp",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.TowEmp#towEmpDuties
         * @methodOf lbServices.TowEmp
         * @deprecated Use towEmp.towEmpDuties() instead.
         *
         * @description
         *
         * Queries towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuties = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::get::towEmp::towEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#towEmpDuties.create
         * @methodOf lbServices.TowEmp
         * @deprecated Use towEmp.towEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuties.create = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::create::towEmp::towEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowEmp#towEmpDuties.destroyAll
         * @methodOf lbServices.TowEmp
         * @deprecated Use towEmp.towEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuties.destroyAll = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::delete::towEmp::towEmpDuties"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TowDuty
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TowDuty` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TowDuty",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/towDuties/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#create
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/towDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#updateOrCreate
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/towDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#upsert
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/towDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#exists
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/towDuties/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#findById
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/towDuties/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#find
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/towDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#findOne
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/towDuties/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#destroyById
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/towDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#deleteById
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/towDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#removeById
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/towDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#count
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/towDuties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#prototype$updateAttributes
         * @methodOf lbServices.TowDuty
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/towDuties/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#prototype$__get__towEmpDuties
         * @methodOf lbServices.TowDuty
         * @deprecated Use towDuty.towEmpDuties() instead.
         *
         * @description
         *
         * Queries towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "prototype$__get__towEmpDuties": {
          url: urlBase + "/towDuties/:id/towEmpDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#prototype$__create__towEmpDuties
         * @methodOf lbServices.TowDuty
         * @deprecated Use towDuty.towEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "prototype$__create__towEmpDuties": {
          url: urlBase + "/towDuties/:id/towEmpDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#prototype$__delete__towEmpDuties
         * @methodOf lbServices.TowDuty
         * @deprecated Use towDuty.towEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        "prototype$__delete__towEmpDuties": {
          url: urlBase + "/towDuties/:id/towEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use towEmpDuty.towDuty() instead.
        "::get::towEmpDuty::towDuty": {
          url: urlBase + "/towEmpDuties/:id/towDuty",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.TowDuty#towEmpDuties
         * @methodOf lbServices.TowDuty
         * @deprecated Use towDuty.towEmpDuties() instead.
         *
         * @description
         *
         * Queries towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuties = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::get::towDuty::towEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#towEmpDuties.create
         * @methodOf lbServices.TowDuty
         * @deprecated Use towDuty.towEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuties.create = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::create::towDuty::towEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowDuty#towEmpDuties.destroyAll
         * @methodOf lbServices.TowDuty
         * @deprecated Use towDuty.towEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuties.destroyAll = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::delete::towDuty::towEmpDuties"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TowEmpDuty
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TowEmpDuty` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TowEmpDuty",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/towEmpDuties/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#create
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/towEmpDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#updateOrCreate
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/towEmpDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#upsert
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/towEmpDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#exists
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/towEmpDuties/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#findById
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/towEmpDuties/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#find
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/towEmpDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#findOne
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/towEmpDuties/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#destroyById
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/towEmpDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#deleteById
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/towEmpDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#removeById
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/towEmpDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#count
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/towEmpDuties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#prototype$updateAttributes
         * @methodOf lbServices.TowEmpDuty
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/towEmpDuties/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#prototype$__get__towDuty
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.towDuty() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towDuty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "prototype$__get__towDuty": {
          url: urlBase + "/towEmpDuties/:id/towDuty",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#prototype$__get__towEmp
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.towEmp() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towEmp
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "prototype$__get__towEmp": {
          url: urlBase + "/towEmpDuties/:id/towEmp",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#prototype$__get__towSite
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.towSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "prototype$__get__towSite": {
          url: urlBase + "/towEmpDuties/:id/towSite",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#prototype$__get__statusEntries
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.statusEntries() instead.
         *
         * @description
         *
         * Queries statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "prototype$__get__statusEntries": {
          url: urlBase + "/towEmpDuties/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#prototype$__create__statusEntries
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.statusEntries.create() instead.
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "prototype$__create__statusEntries": {
          url: urlBase + "/towEmpDuties/:id/statusEntries",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#prototype$__delete__statusEntries
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.statusEntries.destroyAll() instead.
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        "prototype$__delete__statusEntries": {
          url: urlBase + "/towEmpDuties/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use towSite.towEmpDuties() instead.
        "::get::towSite::towEmpDuties": {
          url: urlBase + "/towSites/:id/towEmpDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use towSite.towEmpDuties.create() instead.
        "::create::towSite::towEmpDuties": {
          url: urlBase + "/towSites/:id/towEmpDuties",
          method: "POST",
        },

        // INTERNAL. Use towSite.towEmpDuties.destroyAll() instead.
        "::delete::towSite::towEmpDuties": {
          url: urlBase + "/towSites/:id/towEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use towEmp.towEmpDuties() instead.
        "::get::towEmp::towEmpDuties": {
          url: urlBase + "/towEmps/:id/towEmpDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use towEmp.towEmpDuties.create() instead.
        "::create::towEmp::towEmpDuties": {
          url: urlBase + "/towEmps/:id/towEmpDuties",
          method: "POST",
        },

        // INTERNAL. Use towEmp.towEmpDuties.destroyAll() instead.
        "::delete::towEmp::towEmpDuties": {
          url: urlBase + "/towEmps/:id/towEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use towDuty.towEmpDuties() instead.
        "::get::towDuty::towEmpDuties": {
          url: urlBase + "/towDuties/:id/towEmpDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use towDuty.towEmpDuties.create() instead.
        "::create::towDuty::towEmpDuties": {
          url: urlBase + "/towDuties/:id/towEmpDuties",
          method: "POST",
        },

        // INTERNAL. Use towDuty.towEmpDuties.destroyAll() instead.
        "::delete::towDuty::towEmpDuties": {
          url: urlBase + "/towDuties/:id/towEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use statusEntry.towEmpDuty() instead.
        "::get::statusEntry::towEmpDuty": {
          url: urlBase + "/statusEntries/:id/towEmpDuty",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#towDuty
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.towDuty() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towDuty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowDuty` object.)
         * </em>
         */
        R.towDuty = function() {
          var TargetResource = $injector.get("TowDuty");
          var action = TargetResource["::get::towEmpDuty::towDuty"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#towEmp
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.towEmp() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towEmp
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmp` object.)
         * </em>
         */
        R.towEmp = function() {
          var TargetResource = $injector.get("TowEmp");
          var action = TargetResource["::get::towEmpDuty::towEmp"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#towSite
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.towSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        R.towSite = function() {
          var TargetResource = $injector.get("TowSite");
          var action = TargetResource["::get::towEmpDuty::towSite"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#statusEntries
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.statusEntries() instead.
         *
         * @description
         *
         * Queries statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::get::towEmpDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#statusEntries.create
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.statusEntries.create() instead.
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.create = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::create::towEmpDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowEmpDuty#statusEntries.destroyAll
         * @methodOf lbServices.TowEmpDuty
         * @deprecated Use towEmpDuty.statusEntries.destroyAll() instead.
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.destroyAll = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::delete::towEmpDuty::statusEntries"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.VehicleType
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `VehicleType` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "VehicleType",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/vehicleTypes/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#create
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/vehicleTypes",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#updateOrCreate
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/vehicleTypes",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#upsert
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/vehicleTypes",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#exists
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/vehicleTypes/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#findById
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/vehicleTypes/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#find
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/vehicleTypes",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#findOne
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/vehicleTypes/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#destroyById
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/vehicleTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#deleteById
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/vehicleTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#removeById
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/vehicleTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#count
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/vehicleTypes/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#prototype$updateAttributes
         * @methodOf lbServices.VehicleType
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/vehicleTypes/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#prototype$__get__vehicles
         * @methodOf lbServices.VehicleType
         * @deprecated Use vehicleType.vehicles() instead.
         *
         * @description
         *
         * Queries vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "prototype$__get__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#prototype$__create__vehicles
         * @methodOf lbServices.VehicleType
         * @deprecated Use vehicleType.vehicles.create() instead.
         *
         * @description
         *
         * Creates a new instance in vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "prototype$__create__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#prototype$__delete__vehicles
         * @methodOf lbServices.VehicleType
         * @deprecated Use vehicleType.vehicles.destroyAll() instead.
         *
         * @description
         *
         * Deletes all vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        "prototype$__delete__vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "DELETE",
        },

        // INTERNAL. Use vehicle.vehicleType() instead.
        "::get::vehicle::vehicleType": {
          url: urlBase + "/vehicles/:id/vehicleType",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.VehicleType#vehicles
         * @methodOf lbServices.VehicleType
         * @deprecated Use vehicleType.vehicles() instead.
         *
         * @description
         *
         * Queries vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::get::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#vehicles.create
         * @methodOf lbServices.VehicleType
         * @deprecated Use vehicleType.vehicles.create() instead.
         *
         * @description
         *
         * Creates a new instance in vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.create = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::create::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.VehicleType#vehicles.destroyAll
         * @methodOf lbServices.VehicleType
         * @deprecated Use vehicleType.vehicles.destroyAll() instead.
         *
         * @description
         *
         * Deletes all vehicles of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicles.destroyAll = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::delete::vehicleType::vehicles"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Vehicle
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Vehicle` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Vehicle",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/vehicles/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#create
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/vehicles",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#updateOrCreate
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/vehicles",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#upsert
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/vehicles",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#exists
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/vehicles/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#findById
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/vehicles/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#find
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/vehicles",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#findOne
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/vehicles/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#destroyById
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/vehicles/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#deleteById
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/vehicles/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#removeById
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/vehicles/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#count
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/vehicles/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#prototype$updateAttributes
         * @methodOf lbServices.Vehicle
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/vehicles/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#prototype$__get__vehicleType
         * @methodOf lbServices.Vehicle
         * @deprecated Use vehicle.vehicleType() instead.
         *
         * @description
         *
         * Fetches belongsTo relation vehicleType
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "prototype$__get__vehicleType": {
          url: urlBase + "/vehicles/:id/vehicleType",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#prototype$__get__towReqs
         * @methodOf lbServices.Vehicle
         * @deprecated Use vehicle.towReqs() instead.
         *
         * @description
         *
         * Queries towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "prototype$__get__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#prototype$__create__towReqs
         * @methodOf lbServices.Vehicle
         * @deprecated Use vehicle.towReqs.create() instead.
         *
         * @description
         *
         * Creates a new instance in towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "prototype$__create__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#prototype$__delete__towReqs
         * @methodOf lbServices.Vehicle
         * @deprecated Use vehicle.towReqs.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        "prototype$__delete__towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "DELETE",
        },

        // INTERNAL. Use vehicleType.vehicles() instead.
        "::get::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use vehicleType.vehicles.create() instead.
        "::create::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "POST",
        },

        // INTERNAL. Use vehicleType.vehicles.destroyAll() instead.
        "::delete::vehicleType::vehicles": {
          url: urlBase + "/vehicleTypes/:id/vehicles",
          method: "DELETE",
        },

        // INTERNAL. Use towReq.vehicle() instead.
        "::get::towReq::vehicle": {
          url: urlBase + "/towReqs/:id/vehicle",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.Vehicle#vehicleType
         * @methodOf lbServices.Vehicle
         * @deprecated Use vehicle.vehicleType() instead.
         *
         * @description
         *
         * Fetches belongsTo relation vehicleType
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `VehicleType` object.)
         * </em>
         */
        R.vehicleType = function() {
          var TargetResource = $injector.get("VehicleType");
          var action = TargetResource["::get::vehicle::vehicleType"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#towReqs
         * @methodOf lbServices.Vehicle
         * @deprecated Use vehicle.towReqs() instead.
         *
         * @description
         *
         * Queries towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::get::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#towReqs.create
         * @methodOf lbServices.Vehicle
         * @deprecated Use vehicle.towReqs.create() instead.
         *
         * @description
         *
         * Creates a new instance in towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.create = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::create::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Vehicle#towReqs.destroyAll
         * @methodOf lbServices.Vehicle
         * @deprecated Use vehicle.towReqs.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.destroyAll = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::delete::vehicle::towReqs"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.ReqSite
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `ReqSite` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "ReqSite",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/reqSites/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#create
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/reqSites",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#updateOrCreate
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/reqSites",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#upsert
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/reqSites",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#exists
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/reqSites/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#findById
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/reqSites/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#find
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/reqSites",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#findOne
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/reqSites/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#destroyById
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/reqSites/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#deleteById
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/reqSites/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#removeById
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/reqSites/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#count
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/reqSites/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$updateAttributes
         * @methodOf lbServices.ReqSite
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/reqSites/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__get__towSite
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.towSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__get__towSite": {
          url: urlBase + "/reqSites/:id/towSite",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__get__state
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.state() instead.
         *
         * @description
         *
         * Fetches belongsTo relation state
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__get__state": {
          url: urlBase + "/reqSites/:id/state",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__get__parkingSigns
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.parkingSigns() instead.
         *
         * @description
         *
         * Queries parkingSigns of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__get__parkingSigns": {
          url: urlBase + "/reqSites/:id/parkingSigns",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__create__parkingSigns
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.parkingSigns.create() instead.
         *
         * @description
         *
         * Creates a new instance in parkingSigns of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__create__parkingSigns": {
          url: urlBase + "/reqSites/:id/parkingSigns",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__delete__parkingSigns
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.parkingSigns.destroyAll() instead.
         *
         * @description
         *
         * Deletes all parkingSigns of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__delete__parkingSigns": {
          url: urlBase + "/reqSites/:id/parkingSigns",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__get__reqSiteGeos
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqSiteGeos() instead.
         *
         * @description
         *
         * Queries reqSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__get__reqSiteGeos": {
          url: urlBase + "/reqSites/:id/reqSiteGeos",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__create__reqSiteGeos
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqSiteGeos.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__create__reqSiteGeos": {
          url: urlBase + "/reqSites/:id/reqSiteGeos",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__delete__reqSiteGeos
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqSiteGeos.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__delete__reqSiteGeos": {
          url: urlBase + "/reqSites/:id/reqSiteGeos",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__get__towReqs
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.towReqs() instead.
         *
         * @description
         *
         * Queries towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__get__towReqs": {
          url: urlBase + "/reqSites/:id/towReqs",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__create__towReqs
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.towReqs.create() instead.
         *
         * @description
         *
         * Creates a new instance in towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__create__towReqs": {
          url: urlBase + "/reqSites/:id/towReqs",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__delete__towReqs
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.towReqs.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__delete__towReqs": {
          url: urlBase + "/reqSites/:id/towReqs",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__get__reqEmpDuties
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqEmpDuties() instead.
         *
         * @description
         *
         * Queries reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__get__reqEmpDuties": {
          url: urlBase + "/reqSites/:id/reqEmpDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__create__reqEmpDuties
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__create__reqEmpDuties": {
          url: urlBase + "/reqSites/:id/reqEmpDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#prototype$__delete__reqEmpDuties
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        "prototype$__delete__reqEmpDuties": {
          url: urlBase + "/reqSites/:id/reqEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use state.reqSites() instead.
        "::get::state::reqSites": {
          url: urlBase + "/states/:id/reqSites",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use state.reqSites.create() instead.
        "::create::state::reqSites": {
          url: urlBase + "/states/:id/reqSites",
          method: "POST",
        },

        // INTERNAL. Use state.reqSites.destroyAll() instead.
        "::delete::state::reqSites": {
          url: urlBase + "/states/:id/reqSites",
          method: "DELETE",
        },

        // INTERNAL. Use towSite.reqSites() instead.
        "::get::towSite::reqSites": {
          url: urlBase + "/towSites/:id/reqSites",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use towSite.reqSites.create() instead.
        "::create::towSite::reqSites": {
          url: urlBase + "/towSites/:id/reqSites",
          method: "POST",
        },

        // INTERNAL. Use towSite.reqSites.destroyAll() instead.
        "::delete::towSite::reqSites": {
          url: urlBase + "/towSites/:id/reqSites",
          method: "DELETE",
        },

        // INTERNAL. Use parkingSign.reqSite() instead.
        "::get::parkingSign::reqSite": {
          url: urlBase + "/parkingSigns/:id/reqSite",
          method: "GET",
        },

        // INTERNAL. Use reqSiteGeo.reqSite() instead.
        "::get::reqSiteGeo::reqSite": {
          url: urlBase + "/reqSiteGeos/:id/reqSite",
          method: "GET",
        },

        // INTERNAL. Use towReq.reqSite() instead.
        "::get::towReq::reqSite": {
          url: urlBase + "/towReqs/:id/reqSite",
          method: "GET",
        },

        // INTERNAL. Use reqEmpDuty.reqSite() instead.
        "::get::reqEmpDuty::reqSite": {
          url: urlBase + "/reqEmpDuties/:id/reqSite",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.ReqSite#towSite
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.towSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowSite` object.)
         * </em>
         */
        R.towSite = function() {
          var TargetResource = $injector.get("TowSite");
          var action = TargetResource["::get::reqSite::towSite"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#state
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.state() instead.
         *
         * @description
         *
         * Fetches belongsTo relation state
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        R.state = function() {
          var TargetResource = $injector.get("State");
          var action = TargetResource["::get::reqSite::state"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#parkingSigns
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.parkingSigns() instead.
         *
         * @description
         *
         * Queries parkingSigns of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        R.parkingSigns = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::get::reqSite::parkingSigns"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#parkingSigns.create
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.parkingSigns.create() instead.
         *
         * @description
         *
         * Creates a new instance in parkingSigns of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        R.parkingSigns.create = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::create::reqSite::parkingSigns"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#parkingSigns.destroyAll
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.parkingSigns.destroyAll() instead.
         *
         * @description
         *
         * Deletes all parkingSigns of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        R.parkingSigns.destroyAll = function() {
          var TargetResource = $injector.get("ParkingSign");
          var action = TargetResource["::delete::reqSite::parkingSigns"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#reqSiteGeos
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqSiteGeos() instead.
         *
         * @description
         *
         * Queries reqSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        R.reqSiteGeos = function() {
          var TargetResource = $injector.get("ReqSiteGeo");
          var action = TargetResource["::get::reqSite::reqSiteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#reqSiteGeos.create
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqSiteGeos.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        R.reqSiteGeos.create = function() {
          var TargetResource = $injector.get("ReqSiteGeo");
          var action = TargetResource["::create::reqSite::reqSiteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#reqSiteGeos.destroyAll
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqSiteGeos.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqSiteGeos of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        R.reqSiteGeos.destroyAll = function() {
          var TargetResource = $injector.get("ReqSiteGeo");
          var action = TargetResource["::delete::reqSite::reqSiteGeos"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#towReqs
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.towReqs() instead.
         *
         * @description
         *
         * Queries towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::get::reqSite::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#towReqs.create
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.towReqs.create() instead.
         *
         * @description
         *
         * Creates a new instance in towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.create = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::create::reqSite::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#towReqs.destroyAll
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.towReqs.destroyAll() instead.
         *
         * @description
         *
         * Deletes all towReqs of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReqs.destroyAll = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::delete::reqSite::towReqs"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#reqEmpDuties
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqEmpDuties() instead.
         *
         * @description
         *
         * Queries reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqEmpDuties = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::get::reqSite::reqEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#reqEmpDuties.create
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqEmpDuties.create = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::create::reqSite::reqEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqSite#reqEmpDuties.destroyAll
         * @methodOf lbServices.ReqSite
         * @deprecated Use reqSite.reqEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqEmpDuties.destroyAll = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::delete::reqSite::reqEmpDuties"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.ParkingSign
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `ParkingSign` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "ParkingSign",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/parkingSigns/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#create
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/parkingSigns",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#updateOrCreate
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/parkingSigns",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#upsert
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/parkingSigns",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#exists
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/parkingSigns/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#findById
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/parkingSigns/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#find
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/parkingSigns",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#findOne
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/parkingSigns/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#destroyById
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/parkingSigns/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#deleteById
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/parkingSigns/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#removeById
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/parkingSigns/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#count
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/parkingSigns/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#prototype$updateAttributes
         * @methodOf lbServices.ParkingSign
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/parkingSigns/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#prototype$__get__reqSite
         * @methodOf lbServices.ParkingSign
         * @deprecated Use parkingSign.reqSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ParkingSign` object.)
         * </em>
         */
        "prototype$__get__reqSite": {
          url: urlBase + "/parkingSigns/:id/reqSite",
          method: "GET",
        },

        // INTERNAL. Use reqSite.parkingSigns() instead.
        "::get::reqSite::parkingSigns": {
          url: urlBase + "/reqSites/:id/parkingSigns",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use reqSite.parkingSigns.create() instead.
        "::create::reqSite::parkingSigns": {
          url: urlBase + "/reqSites/:id/parkingSigns",
          method: "POST",
        },

        // INTERNAL. Use reqSite.parkingSigns.destroyAll() instead.
        "::delete::reqSite::parkingSigns": {
          url: urlBase + "/reqSites/:id/parkingSigns",
          method: "DELETE",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.ParkingSign#reqSite
         * @methodOf lbServices.ParkingSign
         * @deprecated Use parkingSign.reqSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSite = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::get::parkingSign::reqSite"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.ReqSiteGeo
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `ReqSiteGeo` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "ReqSiteGeo",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/reqSiteGeos/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#create
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/reqSiteGeos",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#updateOrCreate
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/reqSiteGeos",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#upsert
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/reqSiteGeos",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#exists
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/reqSiteGeos/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#findById
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/reqSiteGeos/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#find
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/reqSiteGeos",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#findOne
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/reqSiteGeos/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#destroyById
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/reqSiteGeos/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#deleteById
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/reqSiteGeos/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#removeById
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/reqSiteGeos/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#count
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/reqSiteGeos/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#prototype$updateAttributes
         * @methodOf lbServices.ReqSiteGeo
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/reqSiteGeos/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#prototype$__get__reqSite
         * @methodOf lbServices.ReqSiteGeo
         * @deprecated Use reqSiteGeo.reqSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSiteGeo` object.)
         * </em>
         */
        "prototype$__get__reqSite": {
          url: urlBase + "/reqSiteGeos/:id/reqSite",
          method: "GET",
        },

        // INTERNAL. Use reqSite.reqSiteGeos() instead.
        "::get::reqSite::reqSiteGeos": {
          url: urlBase + "/reqSites/:id/reqSiteGeos",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use reqSite.reqSiteGeos.create() instead.
        "::create::reqSite::reqSiteGeos": {
          url: urlBase + "/reqSites/:id/reqSiteGeos",
          method: "POST",
        },

        // INTERNAL. Use reqSite.reqSiteGeos.destroyAll() instead.
        "::delete::reqSite::reqSiteGeos": {
          url: urlBase + "/reqSites/:id/reqSiteGeos",
          method: "DELETE",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.ReqSiteGeo#reqSite
         * @methodOf lbServices.ReqSiteGeo
         * @deprecated Use reqSiteGeo.reqSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSite = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::get::reqSiteGeo::reqSite"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TowReq
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TowReq` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TowReq",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/towReqs/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.TowReq#create
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/towReqs",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#updateOrCreate
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/towReqs",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#upsert
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/towReqs",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#exists
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/towReqs/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#findById
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/towReqs/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#find
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/towReqs",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#findOne
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/towReqs/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#destroyById
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/towReqs/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#deleteById
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/towReqs/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#removeById
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/towReqs/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#count
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/towReqs/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$updateAttributes
         * @methodOf lbServices.TowReq
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/towReqs/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$__get__vehicle
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.vehicle() instead.
         *
         * @description
         *
         * Fetches belongsTo relation vehicle
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$__get__vehicle": {
          url: urlBase + "/towReqs/:id/vehicle",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$__get__reqSite
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.reqSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$__get__reqSite": {
          url: urlBase + "/towReqs/:id/reqSite",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$__get__statusEntries
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.statusEntries() instead.
         *
         * @description
         *
         * Queries statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$__get__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$__create__statusEntries
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.statusEntries.create() instead.
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$__create__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$__delete__statusEntries
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.statusEntries.destroyAll() instead.
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$__delete__statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$__get__reqPictures
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.reqPictures() instead.
         *
         * @description
         *
         * Queries reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$__get__reqPictures": {
          url: urlBase + "/towReqs/:id/reqPictures",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$__create__reqPictures
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.reqPictures.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$__create__reqPictures": {
          url: urlBase + "/towReqs/:id/reqPictures",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.TowReq#prototype$__delete__reqPictures
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.reqPictures.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        "prototype$__delete__reqPictures": {
          url: urlBase + "/towReqs/:id/reqPictures",
          method: "DELETE",
        },

        // INTERNAL. Use vehicle.towReqs() instead.
        "::get::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use vehicle.towReqs.create() instead.
        "::create::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "POST",
        },

        // INTERNAL. Use vehicle.towReqs.destroyAll() instead.
        "::delete::vehicle::towReqs": {
          url: urlBase + "/vehicles/:id/towReqs",
          method: "DELETE",
        },

        // INTERNAL. Use reqSite.towReqs() instead.
        "::get::reqSite::towReqs": {
          url: urlBase + "/reqSites/:id/towReqs",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use reqSite.towReqs.create() instead.
        "::create::reqSite::towReqs": {
          url: urlBase + "/reqSites/:id/towReqs",
          method: "POST",
        },

        // INTERNAL. Use reqSite.towReqs.destroyAll() instead.
        "::delete::reqSite::towReqs": {
          url: urlBase + "/reqSites/:id/towReqs",
          method: "DELETE",
        },

        // INTERNAL. Use statusEntry.towReq() instead.
        "::get::statusEntry::towReq": {
          url: urlBase + "/statusEntries/:id/towReq",
          method: "GET",
        },

        // INTERNAL. Use reqPicture.towReq() instead.
        "::get::reqPicture::towReq": {
          url: urlBase + "/reqPictures/:id/towReq",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.TowReq#vehicle
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.vehicle() instead.
         *
         * @description
         *
         * Fetches belongsTo relation vehicle
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Vehicle` object.)
         * </em>
         */
        R.vehicle = function() {
          var TargetResource = $injector.get("Vehicle");
          var action = TargetResource["::get::towReq::vehicle"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq#reqSite
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.reqSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSite = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::get::towReq::reqSite"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq#statusEntries
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.statusEntries() instead.
         *
         * @description
         *
         * Queries statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::get::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq#statusEntries.create
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.statusEntries.create() instead.
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.create = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::create::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq#statusEntries.destroyAll
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.statusEntries.destroyAll() instead.
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.destroyAll = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::delete::towReq::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq#reqPictures
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.reqPictures() instead.
         *
         * @description
         *
         * Queries reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        R.reqPictures = function() {
          var TargetResource = $injector.get("ReqPicture");
          var action = TargetResource["::get::towReq::reqPictures"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq#reqPictures.create
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.reqPictures.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        R.reqPictures.create = function() {
          var TargetResource = $injector.get("ReqPicture");
          var action = TargetResource["::create::towReq::reqPictures"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TowReq#reqPictures.destroyAll
         * @methodOf lbServices.TowReq
         * @deprecated Use towReq.reqPictures.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        R.reqPictures.destroyAll = function() {
          var TargetResource = $injector.get("ReqPicture");
          var action = TargetResource["::delete::towReq::reqPictures"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.ReqDuty
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `ReqDuty` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "ReqDuty",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/reqDuties/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#create
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/reqDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#updateOrCreate
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/reqDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#upsert
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/reqDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#exists
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/reqDuties/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#findById
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/reqDuties/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#find
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/reqDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#findOne
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/reqDuties/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#destroyById
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/reqDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#deleteById
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/reqDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#removeById
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/reqDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#count
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/reqDuties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#prototype$updateAttributes
         * @methodOf lbServices.ReqDuty
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/reqDuties/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#prototype$__get__reqoEmpDuties
         * @methodOf lbServices.ReqDuty
         * @deprecated Use reqDuty.reqoEmpDuties() instead.
         *
         * @description
         *
         * Queries reqoEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "prototype$__get__reqoEmpDuties": {
          url: urlBase + "/reqDuties/:id/reqoEmpDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#prototype$__create__reqoEmpDuties
         * @methodOf lbServices.ReqDuty
         * @deprecated Use reqDuty.reqoEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqoEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "prototype$__create__reqoEmpDuties": {
          url: urlBase + "/reqDuties/:id/reqoEmpDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#prototype$__delete__reqoEmpDuties
         * @methodOf lbServices.ReqDuty
         * @deprecated Use reqDuty.reqoEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqoEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        "prototype$__delete__reqoEmpDuties": {
          url: urlBase + "/reqDuties/:id/reqoEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use reqEmpDuty.reqDuty() instead.
        "::get::reqEmpDuty::reqDuty": {
          url: urlBase + "/reqEmpDuties/:id/reqDuty",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#reqoEmpDuties
         * @methodOf lbServices.ReqDuty
         * @deprecated Use reqDuty.reqoEmpDuties() instead.
         *
         * @description
         *
         * Queries reqoEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqoEmpDuties = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::get::reqDuty::reqoEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#reqoEmpDuties.create
         * @methodOf lbServices.ReqDuty
         * @deprecated Use reqDuty.reqoEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqoEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqoEmpDuties.create = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::create::reqDuty::reqoEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqDuty#reqoEmpDuties.destroyAll
         * @methodOf lbServices.ReqDuty
         * @deprecated Use reqDuty.reqoEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqoEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqoEmpDuties.destroyAll = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::delete::reqDuty::reqoEmpDuties"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.ReqEmp
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `ReqEmp` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "ReqEmp",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/reqEmps/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#create
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/reqEmps",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#updateOrCreate
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/reqEmps",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#upsert
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/reqEmps",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#exists
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/reqEmps/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#findById
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/reqEmps/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#find
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/reqEmps",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#findOne
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/reqEmps/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#destroyById
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/reqEmps/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#deleteById
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/reqEmps/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#removeById
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/reqEmps/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#count
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/reqEmps/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#prototype$updateAttributes
         * @methodOf lbServices.ReqEmp
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/reqEmps/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#prototype$__get__reqEmpDuties
         * @methodOf lbServices.ReqEmp
         * @deprecated Use reqEmp.reqEmpDuties() instead.
         *
         * @description
         *
         * Queries reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "prototype$__get__reqEmpDuties": {
          url: urlBase + "/reqEmps/:id/reqEmpDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#prototype$__create__reqEmpDuties
         * @methodOf lbServices.ReqEmp
         * @deprecated Use reqEmp.reqEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "prototype$__create__reqEmpDuties": {
          url: urlBase + "/reqEmps/:id/reqEmpDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#prototype$__delete__reqEmpDuties
         * @methodOf lbServices.ReqEmp
         * @deprecated Use reqEmp.reqEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        "prototype$__delete__reqEmpDuties": {
          url: urlBase + "/reqEmps/:id/reqEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use reqEmpDuty.reqEmp() instead.
        "::get::reqEmpDuty::reqEmp": {
          url: urlBase + "/reqEmpDuties/:id/reqEmp",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#reqEmpDuties
         * @methodOf lbServices.ReqEmp
         * @deprecated Use reqEmp.reqEmpDuties() instead.
         *
         * @description
         *
         * Queries reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqEmpDuties = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::get::reqEmp::reqEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#reqEmpDuties.create
         * @methodOf lbServices.ReqEmp
         * @deprecated Use reqEmp.reqEmpDuties.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqEmpDuties.create = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::create::reqEmp::reqEmpDuties"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqEmp#reqEmpDuties.destroyAll
         * @methodOf lbServices.ReqEmp
         * @deprecated Use reqEmp.reqEmpDuties.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqEmpDuties of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqEmpDuties.destroyAll = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::delete::reqEmp::reqEmpDuties"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.ReqEmpDuty
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `ReqEmpDuty` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "ReqEmpDuty",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/reqEmpDuties/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#create
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/reqEmpDuties",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#updateOrCreate
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/reqEmpDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#upsert
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/reqEmpDuties",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#exists
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/reqEmpDuties/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#findById
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/reqEmpDuties/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#find
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/reqEmpDuties",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#findOne
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/reqEmpDuties/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#destroyById
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/reqEmpDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#deleteById
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/reqEmpDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#removeById
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/reqEmpDuties/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#count
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/reqEmpDuties/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#prototype$updateAttributes
         * @methodOf lbServices.ReqEmpDuty
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/reqEmpDuties/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#prototype$__get__reqSite
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.reqSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "prototype$__get__reqSite": {
          url: urlBase + "/reqEmpDuties/:id/reqSite",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#prototype$__get__reqDuty
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.reqDuty() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqDuty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "prototype$__get__reqDuty": {
          url: urlBase + "/reqEmpDuties/:id/reqDuty",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#prototype$__get__reqEmp
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.reqEmp() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqEmp
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "prototype$__get__reqEmp": {
          url: urlBase + "/reqEmpDuties/:id/reqEmp",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#prototype$__get__statusEntries
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.statusEntries() instead.
         *
         * @description
         *
         * Queries statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "prototype$__get__statusEntries": {
          url: urlBase + "/reqEmpDuties/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#prototype$__create__statusEntries
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.statusEntries.create() instead.
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "prototype$__create__statusEntries": {
          url: urlBase + "/reqEmpDuties/:id/statusEntries",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#prototype$__delete__statusEntries
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.statusEntries.destroyAll() instead.
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        "prototype$__delete__statusEntries": {
          url: urlBase + "/reqEmpDuties/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use reqSite.reqEmpDuties() instead.
        "::get::reqSite::reqEmpDuties": {
          url: urlBase + "/reqSites/:id/reqEmpDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use reqSite.reqEmpDuties.create() instead.
        "::create::reqSite::reqEmpDuties": {
          url: urlBase + "/reqSites/:id/reqEmpDuties",
          method: "POST",
        },

        // INTERNAL. Use reqSite.reqEmpDuties.destroyAll() instead.
        "::delete::reqSite::reqEmpDuties": {
          url: urlBase + "/reqSites/:id/reqEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use reqDuty.reqoEmpDuties() instead.
        "::get::reqDuty::reqoEmpDuties": {
          url: urlBase + "/reqDuties/:id/reqoEmpDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use reqDuty.reqoEmpDuties.create() instead.
        "::create::reqDuty::reqoEmpDuties": {
          url: urlBase + "/reqDuties/:id/reqoEmpDuties",
          method: "POST",
        },

        // INTERNAL. Use reqDuty.reqoEmpDuties.destroyAll() instead.
        "::delete::reqDuty::reqoEmpDuties": {
          url: urlBase + "/reqDuties/:id/reqoEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use reqEmp.reqEmpDuties() instead.
        "::get::reqEmp::reqEmpDuties": {
          url: urlBase + "/reqEmps/:id/reqEmpDuties",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use reqEmp.reqEmpDuties.create() instead.
        "::create::reqEmp::reqEmpDuties": {
          url: urlBase + "/reqEmps/:id/reqEmpDuties",
          method: "POST",
        },

        // INTERNAL. Use reqEmp.reqEmpDuties.destroyAll() instead.
        "::delete::reqEmp::reqEmpDuties": {
          url: urlBase + "/reqEmps/:id/reqEmpDuties",
          method: "DELETE",
        },

        // INTERNAL. Use statusEntry.reqEmpDuty() instead.
        "::get::statusEntry::reqEmpDuty": {
          url: urlBase + "/statusEntries/:id/reqEmpDuty",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#reqSite
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.reqSite() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqSite
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqSite` object.)
         * </em>
         */
        R.reqSite = function() {
          var TargetResource = $injector.get("ReqSite");
          var action = TargetResource["::get::reqEmpDuty::reqSite"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#reqDuty
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.reqDuty() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqDuty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqDuty` object.)
         * </em>
         */
        R.reqDuty = function() {
          var TargetResource = $injector.get("ReqDuty");
          var action = TargetResource["::get::reqEmpDuty::reqDuty"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#reqEmp
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.reqEmp() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqEmp
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmp` object.)
         * </em>
         */
        R.reqEmp = function() {
          var TargetResource = $injector.get("ReqEmp");
          var action = TargetResource["::get::reqEmpDuty::reqEmp"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#statusEntries
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.statusEntries() instead.
         *
         * @description
         *
         * Queries statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::get::reqEmpDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#statusEntries.create
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.statusEntries.create() instead.
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.create = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::create::reqEmpDuty::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqEmpDuty#statusEntries.destroyAll
         * @methodOf lbServices.ReqEmpDuty
         * @deprecated Use reqEmpDuty.statusEntries.destroyAll() instead.
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.destroyAll = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::delete::reqEmpDuty::statusEntries"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Status
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Status` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Status",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/statuses/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Status#create
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/statuses",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#updateOrCreate
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/statuses",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#upsert
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/statuses",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#exists
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/statuses/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#findById
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/statuses/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#find
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/statuses",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#findOne
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/statuses/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#destroyById
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/statuses/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#deleteById
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/statuses/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#removeById
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/statuses/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#count
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/statuses/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#prototype$updateAttributes
         * @methodOf lbServices.Status
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/statuses/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#prototype$__get__statusEntries
         * @methodOf lbServices.Status
         * @deprecated Use status.statusEntries() instead.
         *
         * @description
         *
         * Queries statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "prototype$__get__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#prototype$__create__statusEntries
         * @methodOf lbServices.Status
         * @deprecated Use status.statusEntries.create() instead.
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "prototype$__create__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Status#prototype$__delete__statusEntries
         * @methodOf lbServices.Status
         * @deprecated Use status.statusEntries.destroyAll() instead.
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        "prototype$__delete__statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use statusEntry.status() instead.
        "::get::statusEntry::status": {
          url: urlBase + "/statusEntries/:id/status",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.Status#statusEntries
         * @methodOf lbServices.Status
         * @deprecated Use status.statusEntries() instead.
         *
         * @description
         *
         * Queries statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::get::status::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Status#statusEntries.create
         * @methodOf lbServices.Status
         * @deprecated Use status.statusEntries.create() instead.
         *
         * @description
         *
         * Creates a new instance in statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.create = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::create::status::statusEntries"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Status#statusEntries.destroyAll
         * @methodOf lbServices.Status
         * @deprecated Use status.statusEntries.destroyAll() instead.
         *
         * @description
         *
         * Deletes all statusEntries of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        R.statusEntries.destroyAll = function() {
          var TargetResource = $injector.get("StatusEntry");
          var action = TargetResource["::delete::status::statusEntries"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.StatusEntry
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `StatusEntry` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "StatusEntry",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/statusEntries/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#create
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/statusEntries",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#updateOrCreate
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/statusEntries",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#upsert
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/statusEntries",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#exists
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/statusEntries/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#findById
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/statusEntries/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#find
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/statusEntries",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#findOne
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/statusEntries/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#destroyById
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/statusEntries/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#deleteById
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/statusEntries/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#removeById
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/statusEntries/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#count
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/statusEntries/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#prototype$updateAttributes
         * @methodOf lbServices.StatusEntry
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/statusEntries/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#prototype$__get__towEmpDuty
         * @methodOf lbServices.StatusEntry
         * @deprecated Use statusEntry.towEmpDuty() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towEmpDuty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "prototype$__get__towEmpDuty": {
          url: urlBase + "/statusEntries/:id/towEmpDuty",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#prototype$__get__reqEmpDuty
         * @methodOf lbServices.StatusEntry
         * @deprecated Use statusEntry.reqEmpDuty() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqEmpDuty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "prototype$__get__reqEmpDuty": {
          url: urlBase + "/statusEntries/:id/reqEmpDuty",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#prototype$__get__status
         * @methodOf lbServices.StatusEntry
         * @deprecated Use statusEntry.status() instead.
         *
         * @description
         *
         * Fetches belongsTo relation status
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "prototype$__get__status": {
          url: urlBase + "/statusEntries/:id/status",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#prototype$__get__towReq
         * @methodOf lbServices.StatusEntry
         * @deprecated Use statusEntry.towReq() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towReq
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StatusEntry` object.)
         * </em>
         */
        "prototype$__get__towReq": {
          url: urlBase + "/statusEntries/:id/towReq",
          method: "GET",
        },

        // INTERNAL. Use towEmpDuty.statusEntries() instead.
        "::get::towEmpDuty::statusEntries": {
          url: urlBase + "/towEmpDuties/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use towEmpDuty.statusEntries.create() instead.
        "::create::towEmpDuty::statusEntries": {
          url: urlBase + "/towEmpDuties/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use towEmpDuty.statusEntries.destroyAll() instead.
        "::delete::towEmpDuty::statusEntries": {
          url: urlBase + "/towEmpDuties/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use towReq.statusEntries() instead.
        "::get::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use towReq.statusEntries.create() instead.
        "::create::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use towReq.statusEntries.destroyAll() instead.
        "::delete::towReq::statusEntries": {
          url: urlBase + "/towReqs/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use reqEmpDuty.statusEntries() instead.
        "::get::reqEmpDuty::statusEntries": {
          url: urlBase + "/reqEmpDuties/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use reqEmpDuty.statusEntries.create() instead.
        "::create::reqEmpDuty::statusEntries": {
          url: urlBase + "/reqEmpDuties/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use reqEmpDuty.statusEntries.destroyAll() instead.
        "::delete::reqEmpDuty::statusEntries": {
          url: urlBase + "/reqEmpDuties/:id/statusEntries",
          method: "DELETE",
        },

        // INTERNAL. Use status.statusEntries() instead.
        "::get::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use status.statusEntries.create() instead.
        "::create::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "POST",
        },

        // INTERNAL. Use status.statusEntries.destroyAll() instead.
        "::delete::status::statusEntries": {
          url: urlBase + "/statuses/:id/statusEntries",
          method: "DELETE",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#towEmpDuty
         * @methodOf lbServices.StatusEntry
         * @deprecated Use statusEntry.towEmpDuty() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towEmpDuty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowEmpDuty` object.)
         * </em>
         */
        R.towEmpDuty = function() {
          var TargetResource = $injector.get("TowEmpDuty");
          var action = TargetResource["::get::statusEntry::towEmpDuty"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#reqEmpDuty
         * @methodOf lbServices.StatusEntry
         * @deprecated Use statusEntry.reqEmpDuty() instead.
         *
         * @description
         *
         * Fetches belongsTo relation reqEmpDuty
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqEmpDuty` object.)
         * </em>
         */
        R.reqEmpDuty = function() {
          var TargetResource = $injector.get("ReqEmpDuty");
          var action = TargetResource["::get::statusEntry::reqEmpDuty"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#status
         * @methodOf lbServices.StatusEntry
         * @deprecated Use statusEntry.status() instead.
         *
         * @description
         *
         * Fetches belongsTo relation status
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Status` object.)
         * </em>
         */
        R.status = function() {
          var TargetResource = $injector.get("Status");
          var action = TargetResource["::get::statusEntry::status"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.StatusEntry#towReq
         * @methodOf lbServices.StatusEntry
         * @deprecated Use statusEntry.towReq() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towReq
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReq = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::get::statusEntry::towReq"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.ReqPicture
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `ReqPicture` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "ReqPicture",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/reqPictures/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#create
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/reqPictures",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#updateOrCreate
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/reqPictures",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#upsert
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/reqPictures",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#exists
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/reqPictures/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#findById
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/reqPictures/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#find
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/reqPictures",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#findOne
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/reqPictures/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#destroyById
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/reqPictures/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#deleteById
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/reqPictures/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#removeById
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/reqPictures/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#count
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/reqPictures/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#prototype$updateAttributes
         * @methodOf lbServices.ReqPicture
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/reqPictures/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#prototype$__get__towReq
         * @methodOf lbServices.ReqPicture
         * @deprecated Use reqPicture.towReq() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towReq
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        "prototype$__get__towReq": {
          url: urlBase + "/reqPictures/:id/towReq",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#prototype$__get__pictureType
         * @methodOf lbServices.ReqPicture
         * @deprecated Use reqPicture.pictureType() instead.
         *
         * @description
         *
         * Fetches belongsTo relation pictureType
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        "prototype$__get__pictureType": {
          url: urlBase + "/reqPictures/:id/pictureType",
          method: "GET",
        },

        // INTERNAL. Use towReq.reqPictures() instead.
        "::get::towReq::reqPictures": {
          url: urlBase + "/towReqs/:id/reqPictures",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use towReq.reqPictures.create() instead.
        "::create::towReq::reqPictures": {
          url: urlBase + "/towReqs/:id/reqPictures",
          method: "POST",
        },

        // INTERNAL. Use towReq.reqPictures.destroyAll() instead.
        "::delete::towReq::reqPictures": {
          url: urlBase + "/towReqs/:id/reqPictures",
          method: "DELETE",
        },

        // INTERNAL. Use pictureType.reqPictures() instead.
        "::get::pictureType::reqPictures": {
          url: urlBase + "/pictureTypes/:id/reqPictures",
          method: "GET",
          isArray: true,
        },

        // INTERNAL. Use pictureType.reqPictures.create() instead.
        "::create::pictureType::reqPictures": {
          url: urlBase + "/pictureTypes/:id/reqPictures",
          method: "POST",
        },

        // INTERNAL. Use pictureType.reqPictures.destroyAll() instead.
        "::delete::pictureType::reqPictures": {
          url: urlBase + "/pictureTypes/:id/reqPictures",
          method: "DELETE",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#towReq
         * @methodOf lbServices.ReqPicture
         * @deprecated Use reqPicture.towReq() instead.
         *
         * @description
         *
         * Fetches belongsTo relation towReq
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TowReq` object.)
         * </em>
         */
        R.towReq = function() {
          var TargetResource = $injector.get("TowReq");
          var action = TargetResource["::get::reqPicture::towReq"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.ReqPicture#pictureType
         * @methodOf lbServices.ReqPicture
         * @deprecated Use reqPicture.pictureType() instead.
         *
         * @description
         *
         * Fetches belongsTo relation pictureType
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        R.pictureType = function() {
          var TargetResource = $injector.get("PictureType");
          var action = TargetResource["::get::reqPicture::pictureType"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.PictureType
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `PictureType` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "PictureType",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/pictureTypes/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.PictureType#create
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/pictureTypes",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#updateOrCreate
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "updateOrCreate": {
          url: urlBase + "/pictureTypes",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#upsert
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/pictureTypes",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#exists
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{*=}` - 
         */
        "exists": {
          url: urlBase + "/pictureTypes/:id/exists",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#findById
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/pictureTypes/:id",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#find
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "find": {
          url: urlBase + "/pictureTypes",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#findOne
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/pictureTypes/findOne",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#destroyById
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "destroyById": {
          url: urlBase + "/pictureTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#deleteById
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/pictureTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#removeById
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "removeById": {
          url: urlBase + "/pictureTypes/:id",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#count
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/pictureTypes/count",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#prototype$updateAttributes
         * @methodOf lbServices.PictureType
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/pictureTypes/:id",
          method: "PUT",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#prototype$__get__reqPictures
         * @methodOf lbServices.PictureType
         * @deprecated Use pictureType.reqPictures() instead.
         *
         * @description
         *
         * Queries reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "prototype$__get__reqPictures": {
          url: urlBase + "/pictureTypes/:id/reqPictures",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#prototype$__create__reqPictures
         * @methodOf lbServices.PictureType
         * @deprecated Use pictureType.reqPictures.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "prototype$__create__reqPictures": {
          url: urlBase + "/pictureTypes/:id/reqPictures",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.PictureType#prototype$__delete__reqPictures
         * @methodOf lbServices.PictureType
         * @deprecated Use pictureType.reqPictures.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PictureType` object.)
         * </em>
         */
        "prototype$__delete__reqPictures": {
          url: urlBase + "/pictureTypes/:id/reqPictures",
          method: "DELETE",
        },

        // INTERNAL. Use reqPicture.pictureType() instead.
        "::get::reqPicture::pictureType": {
          url: urlBase + "/reqPictures/:id/pictureType",
          method: "GET",
        },
      }
    );


        /**
         * @ngdoc method
         * @name lbServices.PictureType#reqPictures
         * @methodOf lbServices.PictureType
         * @deprecated Use pictureType.reqPictures() instead.
         *
         * @description
         *
         * Queries reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        R.reqPictures = function() {
          var TargetResource = $injector.get("ReqPicture");
          var action = TargetResource["::get::pictureType::reqPictures"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PictureType#reqPictures.create
         * @methodOf lbServices.PictureType
         * @deprecated Use pictureType.reqPictures.create() instead.
         *
         * @description
         *
         * Creates a new instance in reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        R.reqPictures.create = function() {
          var TargetResource = $injector.get("ReqPicture");
          var action = TargetResource["::create::pictureType::reqPictures"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PictureType#reqPictures.destroyAll
         * @methodOf lbServices.PictureType
         * @deprecated Use pictureType.reqPictures.destroyAll() instead.
         *
         * @description
         *
         * Deletes all reqPictures of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `ReqPicture` object.)
         * </em>
         */
        R.reqPictures.destroyAll = function() {
          var TargetResource = $injector.get("ReqPicture");
          var action = TargetResource["::delete::pictureType::reqPictures"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Container
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Container` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Container",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/containers/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Container#getContainers
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getContainers": {
          url: urlBase + "/containers",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#createContainer
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "createContainer": {
          url: urlBase + "/containers",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#destroyContainer
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "destroyContainer": {
          url: urlBase + "/containers/:container",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#getContainer
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getContainer": {
          url: urlBase + "/containers/:container",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#getFiles
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {Function(Array.<Object>, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getFiles": {
          url: urlBase + "/containers/:container/files",
          method: "GET",
          isArray: true,
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#getFile
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getFile": {
          url: urlBase + "/containers/:container/files/:file",
          method: "GET",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#removeFile
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "removeFile": {
          url: urlBase + "/containers/:container/files/:file",
          method: "DELETE",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#upload
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `req` – `{object=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `result` – `{object=}` - 
         */
        "upload": {
          url: urlBase + "/containers/:container/upload",
          method: "POST",
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#download
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {Function(Object, Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {Function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @return {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "download": {
          url: urlBase + "/containers/:container/download/:file",
          method: "GET",
        },
      }
    );


    return R;
  }]);


module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId'];

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.rememberMe = undefined;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      var key = '$LoopBack$' + name;
      if (value == null) value = '';
      storage[key] = value;
    }

    function load(name) {
      var key = '$LoopBack$' + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', [ '$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {
          if (LoopBackAuth.accessTokenId) {
            config.headers.authorization = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 } },
              status: 401,
              config: config,
              headers: function() { return undefined; }
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        }
      }
    }])
  .factory('LoopBackResource', [ '$resource', function($resource) {
    return function(url, params, actions) {
      var resource = $resource(url, params, actions);

      // Angular always calls POST on $save()
      // This hack is based on
      // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
      resource.prototype.$save = function(success, error) {
        // Fortunately, LoopBack provides a convenient `upsert` method
        // that exactly fits our needs.
        var result = resource.upsert.call(this, {}, this, success, error);
        return result.$promise || result;
      }

      return resource;
    };
  }]);

})(window, window.angular);