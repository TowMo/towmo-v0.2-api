var toggleBounce = function() {
   if (this.getAnimation() != null) {
      this.setAnimation(!this.getAnimation());
   } else {
      this.setAnimation(google.maps.Animation.BOUNCE);
   }
}

var initialize = function(locData) {
   
   //DECLARE ARRAY OF GEOLOCATIONS FOR ALL TOW REQUESTS
   var geoLocations = [
      [locData.coords.latitude, locData.coords.longitude],
      [37.4588187,-122.1355323],
      [37.4611181,-122.1355966]
   ];



   var displayMap(geoObj) {
      //1 - CONFIGURE THE MAP PROPERTIES
      var mapOptions = {
         center: new google.maps.LatLng(locData.coords.latitude, locData.coords.longitude),
         zoom: 15,
         mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      //2 - CREATE THE MAP IN THE RENDERING PAGE AND WITH THE MAP CONFIGURATIONS
      var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
      var counter = 0;

      //3 - CREATE EACH MAP MARKER OBJECT -- ADD TO THE CREATED MAP
      var locations = _.map(geoLocations, function(geoLocation) {
         var marker = new google.maps.Marker({
            position: new google.maps.LatLng(geoLocation[0], geoLocation[1]),
            map: map,
            animation: google.maps.Animation.DROP
         });

         google.maps.event.addListener(marker, 'mouseover', toggleBounce);
         counter++;

         return {
            request_id: counter,
            marker: this.marker
         };
      });   
   }
   
}

// google.maps.event.addDomListener(window, 'load', navigator.geolocation.getCurrentPosition(initialize));