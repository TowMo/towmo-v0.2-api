# bash
if [ "$(uname)" = "Darwin" ]; then
sed -ie "s+path.join(__dirname, './privatekey.pem')+'/usr/local/share/ca-certificates/private.nopass.key'+g" ./ssl-config.js
sed -ie "s+path.join(__dirname, './certificate.pem')+'/usr/local/share/ca-certificates/rapidssl_1.pem'+g" ./ssl-config.js
sed -ie "s+''+fs.readFileSync('/usr/local/share/ca-certificates/rapidssl_2.pem').toString();+g" ./ssl-config.js
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
sed -i "s+path.join(__dirname, './privatekey.pem')+'/usr/local/share/ca-certificates/private.nopass.key'+g" ./ssl-config.js
sed -i "s+path.join(__dirname, './certificate.pem')+'/usr/local/share/ca-certificates/rapidssl_1.pem'+g" ./ssl-config.js
sed -i "s+''+fs.readFileSync('/usr/local/share/ca-certificates/rapidssl_2.pem').toString();+g" ./ssl-config.js
elif [ "$(expr substr $(uname -s) 1 10)" = "MINGW32_NT" ]; then
sed -i "s+path.join(__dirname, './privatekey.pem')+'/usr/local/share/ca-certificates/private.nopass.key'+g" ./ssl-config.js
sed -i "s+path.join(__dirname, './certificate.pem')+'/usr/local/share/ca-certificates/rapidssl_1.pem'+g" ./ssl-config.js
sed -i "s+''+fs.readFileSync('/usr/local/share/ca-certificates/rapidssl_2.pem').toString();+g" ./ssl-config.js
fi
