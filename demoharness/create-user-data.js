// This is a demo harness to create initial test user data
// THIS MUST BE RETIRED IN PRODUCTION!

var app = require('../server');
var dataSource = app.dataSources.mysqlCreds;
var Account = app.models.User;
var accounts = 
	[
		{
			id: 1,
			email: 'nobody@gmail.com',
			username: 'nobody',
			password: 'nobody',
			firstName: 'nobody',
			middleName: null,
			lastName: 'nobody'
		}, {
			id: 2,
			email: 'cjimenez@liveoaksuper.com',
			username: 'cjimenez',
			password: 'cjimenez',
			firstName: 'Carlos',
			middleName: null,
			lastName: 'Jimenez'
		}, {
			id: 3,
			email: 'mjimenez@liveoaksuper.com',
			username: 'mjimenez',
			password: 'mjimenez',
			firstName: 'Manuel',
			middleName: null,
			lastName: 'Jimenez'
		}, {
			id: 4,
			email: 'bgonzalez@gonzalez-towing.com',
			username: 'bgonzalez',
			password: 'bgonzalez',
			firstName: 'Brad',
			middleName: null,
			lastName: 'Gonzalez'
		}, {
			id: 5,
			email: 'tgonzalez@gonzalez-towing.com',
			username: 'tgonzalez',
			password: 'tgonzalez',
			firstName: 'Tony',
			middleName: null,
			lastName: 'Gonzalez'
		}, {
			id: 6,
			email: 'jgonzalez@gonzalez-towing.com',
			username: 'jgonzalez',
			password: 'jgonzalez',
			firstName: 'Juan',
			middleName: null,
			lastName: 'Gonzalez'
		}, {
			id: 7,
			email: 'ggonzalez@gonzalez-towing.com',
			username: 'ggonzalez',
			password: 'ggonzalez',
			firstName: 'Gary',
			middleName: null,
			lastName: 'Gonzalez'
		}, {
			id: 8,
			email: 'ygonzalez@gonzalez-towing.com',
			username: 'ygonzalez',
			password: 'ygonzalez',
			firstName: 'Ynes',
			middleName: null,
			lastName: 'Gonzalez'
		}, {
			id: 9,
			email: 'mfoy@rainbowcarpetonesantacruz.com',
			username: 'mfoy',
			password: 'mfoy',
			firstName: 'Mike',
			middleName: null,
			lastName: 'Foy'
		},{
			id: 10,
			email: 'afoley@speedoflighttowing.com',
			username: 'afoley',
			password: 'afoley',
			firstName: 'Axel',
			middleName: null,
			lastName: 'Foley'
		}, {
			id: 11,
			email: 'krafferty@crowsnest-santacruz.com',
			username: 'krafferty',
			password: 'krafferty',
			firstName: 'Kevin',
			middleName: null,
			lastName: 'Rafferty'
		}
	];

var count = accounts.length;
dataSource.autoupdate('User', function (err) {
  accounts.forEach(function(act) {
    Account.create(act, function(err, result) {
      if(!err) {
        console.log('Record created:', result);
        count--;
        if(count === 0) {
          console.log('done');
          dataSource.disconnect();
        }
      }
    });
  });
});
