#!/bin/bash
# make sure that towbyte-dev-server-static has same branch name
#  as towbute-dev-server branch names
branchName=$(git symbolic-ref HEAD | sed -e "s/^refs\/heads\///");
git clone git@bitbucket.org:TowByte/towbyte-dev-server-static.git storage;
cd storage;
git checkout -b $branchName;
rm -rf .git
cd ..;
rm -rf ../storage/emps
rm -rf ../storage/vehicles
cp -r storage/* ../storage;
rm -rf storage;
