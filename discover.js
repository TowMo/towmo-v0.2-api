var app = require('./app');
var dataSource = app.dataSources.mysql;

dataSource.discoverSchema('Site', {owner: 'towByte'}, function (err, schema) {
    console.log(JSON.stringify(schema, null, '  '));
});

dataSource.discoverAndBuildModels('Site', {owner: 'towByte'}, function (err, models) {
    models.Toworg.find(function (err, act) {
        if (err) {
            console.error(err);
        } else {
            console.log(act);
        }
    });
});

dataSource.discoverSchema('State', {owner: 'towByte'}, function (err, schema) {
    console.log(JSON.stringify(schema, null, '  '));
});

dataSource.discoverAndBuildModels('State', {owner: 'towByte'}, function (err, models) {
    models.State.find(function (err, act) {
        if (err) {
            console.error(err);
        } else {
            console.log(act);
        }
    });
});