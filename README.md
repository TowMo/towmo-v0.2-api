### TowServ
TowServ is the backend of the towmo application.

This information is correct upto demo2.

We decided not to serve the frontend files from the static / public folder 
because we wanted to wrap the famous-angular framework with AppGyver's 
Steroids API for native write-once deployment

The server backend has some quirks.

* Backend can be run with or without SSL. For the local testing, run
    ```
    sh runhttp.sh
    ```
* If the server is themis.hyperdeck.net, then the pre-installed and signed 
SSL certificate (certificate provider is rapidSSL) must be used
    ```
    cd private
    sh instssl.sh
    ```
Which will modify the private/ssl-config.js to incorporate the proper 
certs.
* Backend can generate its own self-signed certificate. For that, go to 
demoharness directory and run 
    ```
    cd demoharness
    sh generate-cert.sh
    ```
* Backend can re-generate the pre-set user data. For that run 
    ```
    cd demoharness
    sh create-user-data.sh
    ```
* Backend needs to pull the pre-set static element for the demonstration 
purposes. The static image elements are kept in different repo and must be 
incorporated into API server's publicly served directory via
   ```
   cd demoharness
   sh pull-static.sh
   ```
However, the towserv-static repo must have branch with same name as working 
branch of the towserv, for an example, if it is demo2, then the 
towserv-static must have 'demo2' as its branchname for branch name 
harmonization.


