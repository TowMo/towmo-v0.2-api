var loopback = require('loopback');
var boot = require('loopback-boot');
var path = require('path');
var http = require('http');
var https = require('https');

var app = module.exports = loopback();

// request pre-processing middleware
app.use(loopback.compress());

// -- Add your pre-processing middleware here --

// data storage for pictures
var dss = loopback.createDataSource({
    connector: require('loopback-component-storage'),
    provider: 'filesystem',
    root: path.join(__dirname, 'storage')
});
 
var container = dss.createModel('container');
app.model(container);


// boot scripts mount components like REST API
boot(app, __dirname);

// -- Mount static files here--
// All static middleware should be registered at the end, as all requests
// passing the static middleware are hitting the file system
// Example:
//app.use(loopback.static(path.resolve(__dirname, 'storage')));


// Requests that get this far won't be handled
// by any middleware. Convert them into a 404 error
// that will be handled later down the chain.
app.use(loopback.urlNotFound());

// The ultimate error handler.
app.use(loopback.errorHandler());

app.start = function(httpOnly) {
  if(httpOnly === undefined) {
    httpOnly = process.env.HTTP;
  }
  var server = null;
  if(!httpOnly) {
    var options;
    var sslConfig = require('./private/ssl-config');
    options  = {
      key: sslConfig.privateKey,
      cert: sslConfig.certificate,
      ca: sslConfig.intca
    };
    server = https.createServer(options, app);
  } else {
    server = http.createServer(app);
  }
  server.listen(app.get('port'), function() {
    var baseUrl = (httpOnly? 'http://' : 'https://') + app.get('host') + ':' + app.get('port');
    app.emit('started', baseUrl);
    console.log('LoopBack server listening @ %s%s', baseUrl, '/');
  });
  return server;
};

// start the server if `$ node server.js`
if (require.main === module) {
  app.start();
}
